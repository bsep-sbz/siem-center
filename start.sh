#!/usr/bin/env bash
while ! nc -z db 3306; do sleep 3; done

cd /app/siem-center
mvn spring-boot:run