package com.megatravel.simecenter.rules;
dialect  "mvel"

import java.util.List;

import com.megatravel.siemcenter.alarms.models.Alarm;
import com.megatravel.siemcenter.alarms.services.AlarmService;
import com.megatravel.siemcenter.common.models.FactStatus;
import com.megatravel.siemcenter.common.models.IPAddress;
import com.megatravel.siemcenter.common.models.Platform;
import com.megatravel.siemcenter.common.models.Software;
import com.megatravel.siemcenter.common.models.FactStatus;
import com.megatravel.siemcenter.common.services.LoggingService;
import com.megatravel.siemcenter.common.services.IPAddressService;
import com.megatravel.siemcenter.common.services.SoftwareService;
import com.megatravel.siemcenter.common.services.PlatformService;
import com.megatravel.siemcenter.logs.models.Log;
import com.megatravel.siemcenter.logs.models.LogType;
import com.megatravel.siemcenter.logs.services.LogService;
import com.megatravel.siemcenter.users.models.User;
import com.megatravel.siemcenter.users.services.UserService;

global LogService logService
global AlarmService alarmService
global IPAddressService ipAddressService
global SoftwareService softwareService
global PlatformService platformService
global UserService userService


declare Attack
    priority: Number
    alarm: Alarm
end


rule "#12 DOS attack" salience 8
when
    $logs: List(size >= 50) from collect(
        Log() over window:time(1m)
    )
then
    Alarm $alarm = new Alarm();
    $alarm.setMessage("DOS attack");
    $alarm.setStatus(FactStatus.ACTIVE);
    $alarm.setAssociatedLogs($logs);
    $alarm.setForRule("#12");
    insert(new Attack($logs.size(), $alarm));
    LoggingService.info("Dodat DOS attack");
end


rule "#13 Payment subsystem attack" salience 8
when
    $logs: List(size >= 50) from collect(
        Log(
            message contains "payment"
        ) over window:time(1m)
    )
then
    Alarm $alarm = new Alarm();
    $alarm.setMessage("Payment subsystem attack");
    $alarm.setStatus(FactStatus.ACTIVE);
    $alarm.setAssociatedLogs($logs);
    $alarm.setForRule("#13");
    insert(new Attack($logs.size() * 3, $alarm));
    LoggingService.info("Dodat Payment attack");
end


rule "#14 Brute force attack" salience 8
when
    $logs: List(size >= 50) from collect(
        Log(
            message contains "login"
        ) over window:time(1m)
    )
then
    Alarm $alarm = new Alarm();
    $alarm.setMessage("Brute force attack");
    $alarm.setStatus(FactStatus.ACTIVE);
    $alarm.setAssociatedLogs($logs);
    $alarm.setForRule("#14");
    insert(new Attack($logs.size() * 5, $alarm));
    LoggingService.info("Dodat BF attack");
end

rule "Attack Prioritization rule" salience 7
when
    $attack: Attack($priority: priority)
    not Attack(priority > $priority)
then
    insert($attack.getAlarm());
    LoggingService.info("Alarm raised with message: " + $attack.getAlarm().getMessage());
end