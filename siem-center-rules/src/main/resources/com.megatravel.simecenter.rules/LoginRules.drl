package com.megatravel.simecenter.rules;
dialect  "mvel"

import java.time.LocalDateTime;
import java.util.List;

import com.megatravel.siemcenter.alarms.models.Alarm;
import com.megatravel.siemcenter.alarms.services.AlarmService;
import com.megatravel.siemcenter.common.models.FactStatus;
import com.megatravel.siemcenter.common.models.IPAddress;
import com.megatravel.siemcenter.common.models.Platform;
import com.megatravel.siemcenter.common.models.Software;
import com.megatravel.siemcenter.common.models.FactStatus;
import com.megatravel.siemcenter.common.services.LoggingService;
import com.megatravel.siemcenter.common.services.IPAddressService;
import com.megatravel.siemcenter.common.services.SoftwareService;
import com.megatravel.siemcenter.common.services.PlatformService;
import com.megatravel.siemcenter.logs.models.Log;
import com.megatravel.siemcenter.logs.models.LogType;
import com.megatravel.siemcenter.logs.services.LogService;
import com.megatravel.siemcenter.users.models.User;
import com.megatravel.siemcenter.users.services.UserService;
import com.megatravel.simecenter.rules.LogForRule;

global LogService logService
global AlarmService alarmService
global IPAddressService ipAddressService
global SoftwareService softwareService
global PlatformService platformService
global UserService userService



declare trait FactForRule
    forRule: String
end

declare trait AlarmForIp extends FactForRule
    ip: IPAddress
end

declare trait LogForRule extends FactForRule
    log: Log
end
//rule "User logged in" salience 8
//no-loop
//when
//    $log: Log($metadata: metadata, type == LogType.NOTICE)
//    String(this == "login") from $metadata.get("class")
//then
//    $log.getUser().setLastActive(LocalDateTime.now());
//    update($log);
//end


rule "#1 Failed login attempts on same machine" salience 7
when
    $ip: IPAddress() from ipAddressService.findAll()
    $logs: List(size > 1) from collect(
        // CRITICAL log type indicates failed login attempt
        $log: Log(message contains "login", type == LogType.CRITICAL, ip == $ip)
    )
then
    Alarm $alarm = new Alarm();
    $alarm.setMessage("2 or more failed login attempts on the same machine");
    $alarm.setStatus(FactStatus.ACTIVE);
    $alarm.setForRule("#1");

    for(Log log: $logs) {
        $alarm.getAssociatedLogs().add(log);
    }

    alarmService.save($alarm);

    insert($alarm);
    LoggingService.info("Alarm raised with message: " + $alarm.getMessage());
end


rule "#2 Failed login attempts by the same user" salience 7
when
    $user: User() from userService.findAll()
    $logs: List(size > 1) from collect(
        // CRITICAL log type indicates failed login attempt
        $log: Log(message contains "login", type == LogType.CRITICAL, user == $user)
    )
then
    Alarm $alarm = new Alarm();
    $alarm.setMessage("2 or more failed login attempts by the same user");
    $alarm.setStatus(FactStatus.ACTIVE);
    $alarm.setForRule("#2");

    for(Log log: $logs) {
        $alarm.getAssociatedLogs().add(log);
    }

    alarmService.save($alarm);

    insert($alarm);
    LoggingService.info("Alarm raised with message: " + $alarm.getMessage());
end


rule "#4 Failed login attempt on previously inactive account" salience 7
when
    $user: User(
        lastActive.isBefore(LocalDateTime.now().minusDays(90))
    ) from userService.findAll()
    $log: Log(message contains "login", type == LogType.CRITICAL, user == $user)
then
    Alarm $alarm = new Alarm();
    $alarm.setMessage("Failed login attempt on previously inactive account (more than 90 days)");
    $alarm.setForRule("#4");
    $alarm.setStatus(FactStatus.ACTIVE);
    $alarm.getAssociatedLogs().add($log);
    alarmService.save($alarm);
    insert($alarm);
    LoggingService.info("Alarm raised with message: " + $alarm.getMessage());
end


rule "#5 Failed login attempts from same IP that are close to each other" salience 7
when
    $ip: IPAddress() from ipAddressService.findAll()
    $logs: List(size >= 15) from collect(
        Log(message contains "login", type == LogType.CRITICAL, ip == $ip)
        over window:time(5d)
    )
    not(AlarmForIp(forRule == "#5", ip == $ip))
then
    Alarm $alarm = new Alarm();
    $alarm.setMessage("Failed login attempts from same IP that are close to each other (timespan of 5 days)");
    $alarm.setForRule("#5");
    $alarm.setStatus(FactStatus.ACTIVE);

    for(Log log: $logs) {
        $alarm.getAssociatedLogs().add(log);
    }

    alarmService.save($alarm);

    AlarmForIp $a = don($alarm, AlarmForIp.class);
    $a.setIp($ip);
    $a.setForRule("#5");

    insert($alarm);
    LoggingService.info("Alarm raised with message: " + $alarm.getMessage());
end


rule "#6 Successful logins on different machines in narrow timestamp by the same user" salience 7
when
    $user: User() from userService.findAll()
    $ip: IPAddress() from ipAddressService.findAll()
    $log1: Log(
        ip == $ip,
        message contains "login",
        type == LogType.NOTICE,
        user == $user
    )
    $log2: Log(
        ip != $ip,
        message contains "login",
        type == LogType.NOTICE,
        user == $user,
        timestamp.getValue().isAfter($log1.getTimestamp().getValue().minusSeconds(10)) &&
        timestamp.getValue().isBefore($log1.getTimestamp().getValue().plusSeconds(10))
    )
    not(LogForRule(log == $log1 || log == $log2))
then
    Alarm $alarm = new Alarm();
    $alarm.setMessage("Successful logins on different machines in narrow timestamp by the same user (10 second timespan");
    $alarm.setForRule("#6");
    $alarm.setStatus(FactStatus.ACTIVE);
    $alarm.getAssociatedLogs().add($log1);
    $alarm.getAssociatedLogs().add($log2);
    alarmService.save($alarm);

    LogForRule lfr = don($log1, LogForRule.class);
    lfr.setLog($log1);

    insert($alarm);
    LoggingService.info("Alarm raised with message: " + $alarm.getMessage());
end


rule "#8 Successful login followed by user account change" salience 7
when
    $log1: Log(
        $ip: ip,
        $user: user,
        $ts: timestamp,
        message contains "login",
        type == LogType.NOTICE
    )
    $log2: Log(
        ip == $ip,
        user == $user,
        message contains "account",
        message contains "updated",
        type == LogType.NOTICE,
        timestamp.getValue().isBefore($ts.getValue().plusHours(1))
    )
    $logs: List(size >= 5) from collect(
        Log(
            user != $user,
            message contains "login",
            type == LogType.CRITICAL,
            timestamp.getValue().isAfter($ts.getValue().minusSeconds(90))
        )
    )
then
    Alarm $alarm = new Alarm();
    $alarm.setMessage("Successful login followed by user account change");
    $alarm.setForRule("#8");
    $alarm.setStatus(FactStatus.ACTIVE);
    $alarm.getAssociatedLogs().add($log1);
    $alarm.getAssociatedLogs().add($log2);
    for(Log log: $logs) {
        $alarm.getAssociatedLogs().add(log);
    }

    alarmService.save($alarm);

    insert($alarm);
    LoggingService.info("Alarm raised with message: " + $alarm.getMessage());
end


rule "#10 Login attempt from malicious IP address" salience 7
when
    $log: Log(
        message contains "login",
        type == LogType.NOTICE || type == LogType.CRITICAL,
        ip.malicious == true
    )
then
    Alarm $alarm = new Alarm();
    $alarm.setMessage("Login attempt from malicious IP address");
    $alarm.setForRule("#10");
    $alarm.setStatus(FactStatus.ACTIVE);
    $alarm.getAssociatedLogs().add($log);

    alarmService.save($alarm);

    insert($alarm);
    LoggingService.info("Alarm raised with message: " + $alarm.getMessage());
end