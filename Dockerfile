FROM maven:alpine

RUN apk add --no-cache netcat-openbsd

WORKDIR /app

COPY . .

WORKDIR /app/siem-center-rules
RUN mvn install

WORKDIR /app/siem-center
RUN mvn install

WORKDIR /app
CMD ["./start.sh"]
