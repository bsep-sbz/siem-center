package com.megatravel.siemcenter;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.kie.api.KieBase;
import org.kie.api.definition.KiePackage;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
public class TempTest {

    @Test
    public void testDRLLoading() throws IOException {
        KieHelper kieHelper = new KieHelper();

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(TempTest.class.getClassLoader());

        Resource[] resources = resolver.getResources("classpath:com.megatravel.siemcenter.staticrules/*");
        for (Resource resource: resources) {
            String content = Files.asCharSource(resource.getFile(), Charsets.UTF_8).read();
            kieHelper.addContent(content, ResourceType.DRL);
        }

        KieBase kieBase = kieHelper.build();

        long ruleCount = kieBase.getKiePackages().stream().map(KiePackage::getRules).map(Collection::size).reduce(0, Integer::sum);
        assertEquals(1, ruleCount);

        KieSession session = kieBase.newKieSession();
        session.insert(new String("Test"));
        assertEquals(1, session.getFactCount());

        Collection facts = session.getObjects();

        session = kieBase.newKieSession();
        facts.forEach(session::insert);
        assertEquals(1, session.getFactCount());
    }

    private void addDRL(String drl) {
    }
}
