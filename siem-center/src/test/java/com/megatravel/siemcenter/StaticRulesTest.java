package com.megatravel.siemcenter;

import com.megatravel.siemcenter.alarms.models.Alarm;
import com.megatravel.siemcenter.common.models.RiskFactor;
import com.megatravel.siemcenter.common.services.IPAddressService;
import com.megatravel.siemcenter.logs.models.Log;
import com.megatravel.siemcenter.logs.models.LogType;
import com.megatravel.siemcenter.logs.services.LogService;
import com.megatravel.siemcenter.rules.services.RuleService;
import com.megatravel.siemcenter.users.models.User;
import com.megatravel.siemcenter.users.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
public class StaticRulesTest {

    @Autowired
    private LogService logService;
    @Autowired
    private RuleService ruleService;
    @Autowired
    private UserService userService;
    @Autowired
    private IPAddressService ipAddressService;
    @Autowired
    private MongoTemplate mongoTemplate;

    @Before
    public void setUp() {
        this.ruleService.clearFacts();
        this.mongoTemplate.getDb().drop();
    }

    @Test
    public void Rule_01_FailedLoginAttemptsOnSameMachine() {
        Log log1 = this.logService.save(
            LogType.CRITICAL,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Failed login attempt for user: test",
            LocalDateTime.now().minusMinutes(5)
        );

        Log log2 = this.logService.save(
            LogType.CRITICAL,
            "192.168.1.1",
            "test platform",
            "test software",
            "test 2",
            "Failed login attempt for user: test",
            LocalDateTime.now().minusMinutes(1)
        );

        this.ruleService.insertLog(log1);
        this.ruleService.insertLog(log2);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#1");
        assertEquals(1, alarms.size());
    }

    @Test
    public void Rule_02_FailedLoginAttemptsByTheSameUser() {
        Log log1 = this.logService.save(
            LogType.CRITICAL,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Failed login attempt for user: test",
            LocalDateTime.now().minusMinutes(5)
        );

        Log log2 = this.logService.save(
            LogType.CRITICAL,
            "192.168.1.2",
            "test platform",
            "test software",
            "test",
            "Failed login attempt for user: test",
            LocalDateTime.now().minusMinutes(1)
        );

        this.ruleService.insertLog(log1);
        this.ruleService.insertLog(log2);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#2");
        assertEquals(1, alarms.size());
    }

    @Test
    public void Rule_03_ErrorLog() {
        Log log = this.logService.save(
            LogType.ERROR,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Failed to process user request. Code error 40",
            LocalDateTime.now().minusMinutes(5)
        );

        this.ruleService.insertLog(log);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#3");
        assertEquals(1, alarms.size());
    }

    @Test
    public void Rule_04_FailedLoginAttemptOnInactiveUserAccount() {
        Log log = this.logService.save(
            LogType.CRITICAL,
            "192.168.1.1",
            "test platform",
            "test software",
            "test 3",
            "Failed login attempt for user: test",
            LocalDateTime.now().minusMinutes(5)
        );

        User user = this.userService.findOneByUsername("test 3").get();
        user.setLastActive(LocalDateTime.now().minusDays(91));
        this.userService.save(user);

        this.ruleService.insertLog(log);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#4");
        assertEquals(1, alarms.size());
    }

    @Test
    public void Rule_05_FailedLoginAttemptsFromTheSameIPCloseToEachOther() {
        for(int i = 0; i < 20; i++) {
            Log log = this.logService.save(
                LogType.CRITICAL,
                "192.168.1.1",
                "test platform",
                "test software",
                "test",
                "Failed login attempt for user: test",
                LocalDateTime.now().minusHours(i)
            );
            this.ruleService.insertLog(log);
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#5");
        assertEquals(1, alarms.size());
    }


    @Test
    public void Rule_06_SuccessfulLoginFromDifferentIPAddressesInShortTimespan() {
        Log log1 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Successful login attempt for user: test",
            LocalDateTime.now().minusHours(1)
        );
        Log log2 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.2",
            "test platform",
            "test software",
            "test",
            "Successful login attempt for user: test",
            LocalDateTime.now().minusHours(1).plusSeconds(2)
        );

        this.ruleService.insertLog(log1);
        this.ruleService.insertLog(log2);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#6");
        assertEquals(1, alarms.size());
    }

    @Test
    public void Rule_07_ThreatEliminated() {
        Log log1 = this.logService.save(
            LogType.WARNING,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Virus threat detected",
            LocalDateTime.now().minusHours(2)
        );
        Log log2 = this.logService.save(
            LogType.INFORMATION,
            "192.168.1.2",
            "test platform",
            "test software",
            "test",
            "Virus threat eliminated",
            LocalDateTime.now().minusHours(1).plusMinutes(45)
        );

        this.ruleService.insertLog(log1);
        this.ruleService.insertLog(log2);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#7");
        assertEquals(1, alarms.size());
    }

    @Test
    public void Rule_08_SuccessfulLoginFollowedByAccountUpdate() {
        for(int i = 0; i < 5; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.CRITICAL,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test " + i,
                    "Failed login attempt for user: test " + i,
                    LocalDateTime.now().minusSeconds(45 - i)
                )
            );
        }

        Log log1 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Successful login attempt for user: test",
            LocalDateTime.now().minusSeconds(10)
        );
        Log log2 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "User test updated his/hers account",
            LocalDateTime.now()
        );

        this.ruleService.insertLog(log1);
        this.ruleService.insertLog(log2);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#8");
        assertEquals(1, alarms.size());
    }

    @Test
    public void Rule_09_MultipleThreatsDetectedForTheSameMachine() {
        for (int i = 0; i < 9; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.WARNING,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test",
                    "Virus threat detected",
                    LocalDateTime.now().minusSeconds(45 - i)
                )
            );
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#9");
        assertEquals(1, alarms.size());
    }

    @Test
    public void Rule_10_LoginAttemptFromMaliciousIPAddress() {
        Log log = this.logService.save(
            LogType.NOTICE,
            "192.168.1.125",
            "test platform",
            "test software",
            "test",
            "Successful login attempt for user: test",
            LocalDateTime.now()
        );

        log.getIp().setMalicious(true);
        this.ipAddressService.save(log.getIp());

        this.ruleService.insertLog(log);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#10");
        assertEquals(1, alarms.size());
    }

    @Test
    public void Rule_11_LogFromMaliciousIPAddress() {
        Log log = this.logService.save(
            LogType.INFORMATION,
            "192.168.1.125",
            "test platform",
            "test software",
            "test",
            "Successful operation",
            LocalDateTime.now()
        );

        log.getIp().setMalicious(true);
        this.ipAddressService.save(log.getIp());

        this.ruleService.insertLog(log);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#11");
        assertEquals(1, alarms.size());
    }

    @Test
    public void Rule_12_DOSAttack() {
        for (int i = 0; i < 50; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.WARNING,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test",
                    "Virus threat detected",
                    LocalDateTime.now()
                )
            );
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#12");
        assertEquals(1, alarms.size());

        Alarm alarm = alarms.get(0);
        assertEquals("DOS attack", alarm.getMessage());
    }

    @Test
    public void Rule_13_PaymentSubsystemAttack() {
        for (int i = 0; i < 50; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.ERROR,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test",
                    "payment transaction failed",
                    LocalDateTime.now()
                )
            );
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#13");
        assertEquals(1, alarms.size());

        Alarm alarm = alarms.get(0);
        assertEquals("Payment subsystem attack", alarm.getMessage());
    }

    @Test
    public void Rule_14_BruteForceAttack() {
        for (int i = 0; i < 50; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.CRITICAL,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test",
                    "Failed login attempt for user: test",
                    LocalDateTime.now()
                )
            );
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#14");
        assertEquals(1, alarms.size());

        Alarm alarm = alarms.get(0);
        assertEquals("Brute force attack", alarm.getMessage());
    }

    @Test
    public void Rule_15_UserAssociatedWithThreat() {
        Log log = this.logService.save(
            LogType.WARNING,
            "192.168.1.1",
            "test platform",
            "test software",
            "test a12",
            "Virus threat detected",
            LocalDateTime.now()
        );

        this.ruleService.insertLog(log);

        this.ruleService.execute();

        assertEquals(
            RiskFactor.MODERATE,
            this.userService.findOneByUsername("test a12").get().getRiskFactor()
        );
    }

    @Test
    public void Rule_16_UserHadMultipleFailedLoginAttempts() {
        for (int i = 0; i < 15; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.CRITICAL,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test a13",
                    "Failed login attempt for user: test",
                    LocalDateTime.now().minusDays(15 - i)
                )
            );
        }

        this.ruleService.execute();

        assertEquals(
            RiskFactor.MODERATE,
            this.userService.findOneByUsername("test a13").get().getRiskFactor()
        );
    }

    @Test
    public void Rule_17_UserWithHighRisk() {
        Log log = this.logService.save(
            LogType.ERROR,
            "192.168.1.1",
            "test platform",
            "test software",
            "admin 1",
            "Failed to process request",
            LocalDateTime.now()
        );

        User user = this.userService.findOneByUsername("admin 1").get();
        user.setAdmin(true);
        this.userService.save(user);

        this.ruleService.insertLog(log);

        this.ruleService.execute();

        assertEquals(
            RiskFactor.HIGH,
            this.userService.findOneByUsername("admin 1").get().getRiskFactor()
        );
    }

    @Test
    public void Rule_17_UserWithHighRisk2() {
        Log log1 = this.logService.save(
            LogType.CRITICAL,
            "192.168.1.1",
            "test platform",
            "test software",
            "admin 2",
            "Failed login attempt",
            LocalDateTime.of(2019, 9, 9, 6, 40, 0)
        );

        Log log2 = this.logService.save(
            LogType.CRITICAL,
            "192.168.1.1",
            "test platform",
            "test software",
            "admin 2",
            "Failed login attempt",
            LocalDateTime.of(2019, 9, 9, 6, 45, 0)
        );


        User user = this.userService.findOneByUsername("admin 2").get();
        user.setAdmin(true);
        this.userService.save(user);

        this.ruleService.insertLog(log1);
        this.ruleService.insertLog(log2);

        this.ruleService.execute();

        assertEquals(
            RiskFactor.HIGH,
            this.userService.findOneByUsername("admin 2").get().getRiskFactor()
        );
    }

    @Test
    public void Rule_20_MaliciousIPAddress() {
        for (int i = 0; i < 30; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.CRITICAL,
                    "192.168.1.111",
                    "test platform",
                    "test software",
                    "test a13",
                    "Failed login attempt for user: test",
                    LocalDateTime.now().minusHours(15 - i)
                )
            );
        }

        this.ruleService.execute();

        assertTrue(this.ipAddressService.findByValue("192.168.1.111").getMalicious());
    }

    @Test
    public void Rule_01_FailedLoginAttemptsOnSameMachine_DifferentIPs() {
        Log log1 = this.logService.save(
            LogType.CRITICAL,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Failed login attempt for user: test",
            LocalDateTime.now().minusMinutes(5)
        );

        Log log2 = this.logService.save(
            LogType.CRITICAL,
            "192.168.1.2",
            "test platform",
            "test software",
            "test 2",
            "Failed login attempt for user: test",
            LocalDateTime.now().minusMinutes(1)
        );

        this.ruleService.insertLog(log1);
        this.ruleService.insertLog(log2);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#1");
        assertEquals(0, alarms.size());
    }


    @Test
    public void Rule_02_FailedLoginAttemptsByTheSameUser_DifferentUsers() {
        Log log1 = this.logService.save(
            LogType.CRITICAL,
            "192.168.1.1",
            "test platform",
            "test software",
            "test1",
            "Failed login attempt for user: test1",
            LocalDateTime.now().minusMinutes(5)
        );

        Log log2 = this.logService.save(
            LogType.CRITICAL,
            "192.168.1.2",
            "test platform",
            "test software",
            "test2",
            "Failed login attempt for user: test2",
            LocalDateTime.now().minusMinutes(1)
        );

        this.ruleService.insertLog(log1);
        this.ruleService.insertLog(log2);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#2");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_03_ErrorLog_LogTypeNotError() {
        Log log = this.logService.save(
            LogType.WARNING,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Virus threat detected",
            LocalDateTime.now().minusMinutes(5)
        );

        this.ruleService.insertLog(log);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#3");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_04_FailedLoginAttemptOnInactiveUserAccount_UserInactive89Days() {
        Log log = this.logService.save(
            LogType.CRITICAL,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Failed login attempt for user: test",
            LocalDateTime.now().minusMinutes(5)
        );

        User user = this.userService.findOneByUsername("test").get();
        user.setLastActive(LocalDateTime.now().minusDays(89));
        this.userService.save(user);

        this.ruleService.insertLog(log);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#4");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_04_SuccessfulLoginAttemptOnInactiveUserAccount() {
        Log log = this.logService.save(
            LogType.NOTICE,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Successful login attempt for user: test",
            LocalDateTime.now().minusMinutes(5)
        );

        User user = this.userService.findOneByUsername("test").get();
        user.setLastActive(LocalDateTime.now().minusDays(100));
        this.userService.save(user);

        this.ruleService.insertLog(log);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#4");
        assertEquals(1, alarms.size());
    }

    @Test
    public void Rule_04_SuccessfulLoginAttemptOnInactiveUserAccount_UserInactive89Days() {
        Log log = this.logService.save(
            LogType.NOTICE,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Successful login attempt for user: test",
            LocalDateTime.now().minusMinutes(5)
        );

        User user = this.userService.findOneByUsername("test").get();
        user.setLastActive(LocalDateTime.now().minusDays(89));
        this.userService.save(user);

        this.ruleService.insertLog(log);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#4");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_05_FailedLoginAttemptsFromTheSameIPCloseToEachOther_LessThan15() {
        for(int i = 0; i < 10; i++) {
            Log log = this.logService.save(
                LogType.CRITICAL,
                "192.168.1.1",
                "test platform",
                "test software",
                "test",
                "Failed login attempt for user: test",
                LocalDateTime.now().minusHours(i)
            );
            this.ruleService.insertLog(log);
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#5");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_05_FailedLoginAttemptsFromTheSameIPCloseToEachOther_DifferentIPs() {
        for(int i = 0; i < 20; i++) {
            String ip = "192.168.1.1";
            if (i > 10) {
                ip = "192.168.1.2";
            }
            Log log = this.logService.save(
                LogType.CRITICAL,
                ip,
                "test platform",
                "test software",
                "test",
                "Failed login attempt for user: test",
                LocalDateTime.now().minusHours(i)
            );
            this.ruleService.insertLog(log);
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#5");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_05_FailedLoginAttemptsFromTheSameIPCloseToEachOther_MoreThan5Days() {
        for(int i = 0; i < 20; i++) {
            Log log = this.logService.save(
                LogType.CRITICAL,
                "192.168.1.1",
                "test platform",
                "test software",
                "test",
                "Failed login attempt for user: test",
                LocalDateTime.now().minusDays(i)
            );
            this.ruleService.insertLog(log);
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#5");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_06_SuccessfulLoginFromDifferentIPAddressesInShortTimespan_DifferentUsers() {
        Log log1 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.1",
            "test platform",
            "test software",
            "test1",
            "Successful login attempt for user: test1",
            LocalDateTime.now().minusHours(1)
        );
        Log log2 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.2",
            "test platform",
            "test software",
            "test2",
            "Successful login attempt for user: test2",
            LocalDateTime.now().minusHours(1).plusSeconds(2)
        );

        this.ruleService.insertLog(log1);
        this.ruleService.insertLog(log2);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#6");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_06_SuccessfulLoginFromDifferentIPAddressesInShortTimespan_SameIPs() {
        Log log1 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Successful login attempt for user: test",
            LocalDateTime.now().minusHours(1)
        );
        Log log2 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Successful login attempt for user: test",
            LocalDateTime.now().minusHours(1).plusSeconds(2)
        );

        this.ruleService.insertLog(log1);
        this.ruleService.insertLog(log2);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#6");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_06_SuccessfulLoginFromDifferentIPAddressesInShortTimespan_MoreThan10Seconds() {
        Log log1 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Successful login attempt for user: test",
            LocalDateTime.now().minusHours(1)
        );
        Log log2 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.2",
            "test platform",
            "test software",
            "test",
            "Successful login attempt for user: test",
            LocalDateTime.now().minusHours(1).plusSeconds(20)
        );

        this.ruleService.insertLog(log1);
        this.ruleService.insertLog(log2);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#6");
        assertEquals(0, alarms.size());
    }

    /*  test Rule_07_ThreatEliminated bi mozda trebao biti nazvan Rule_07_ThreatEliminatedTooLate jer je pretnja
        prekasno eliminisana(ne u roku od sat vremena), pa je zato podignut alarm
     */

    @Test
    public void Rule_07_ThreatEliminated_InTime() {
        Log log1 = this.logService.save(
            LogType.WARNING,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Virus threat detected",
            LocalDateTime.now().minusHours(1)
        );
        Log log2 = this.logService.save(
            LogType.INFORMATION,
            "192.168.1.2",
            "test platform",
            "test software",
            "test",
            "Virus threat eliminated",
            LocalDateTime.now().minusMinutes(45)
        );

        this.ruleService.insertLog(log1);
        this.ruleService.insertLog(log2);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#7");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_08_SuccessfulLoginFollowedByAccountUpdate_NoUpdate() {
        for(int i = 0; i < 5; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.CRITICAL,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test " + i,
                    "Failed login attempt for user: test " + i,
                    LocalDateTime.now().minusSeconds(45 - i)
                )
            );
        }

        Log log1 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Successful login attempt for user: test",
            LocalDateTime.now().minusSeconds(10)
        );


        this.ruleService.insertLog(log1);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#8");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_08_SuccessfulLoginFollowedByAccountUpdate_LessThan5Attempts() {
        for(int i = 0; i < 4; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.CRITICAL,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test " + i,
                    "Failed login attempt for user: test " + i,
                    LocalDateTime.now().minusSeconds(45 - i)
                )
            );
        }

        Log log1 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Successful login attempt for user: test",
            LocalDateTime.now().minusSeconds(10)
        );
        Log log2 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "User test updated his/hers account",
            LocalDateTime.now()
        );

        this.ruleService.insertLog(log1);
        this.ruleService.insertLog(log2);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#8");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_08_SuccessfulLoginFollowedByAccountUpdate_SuccessfulLoginFromDifferentIP() {
        for(int i = 0; i < 5; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.CRITICAL,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test " + i,
                    "Failed login attempt for user: test " + i,
                    LocalDateTime.now().minusSeconds(45 - i)
                )
            );
        }

        Log log1 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.2",
            "test platform",
            "test software",
            "test",
            "Successful login attempt for user: test",
            LocalDateTime.now().minusSeconds(10)
        );
        Log log2 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.2",
            "test platform",
            "test software",
            "test",
            "User test updated his/hers account",
            LocalDateTime.now()
        );

        this.ruleService.insertLog(log1);
        this.ruleService.insertLog(log2);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#8");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_08_SuccessfulLoginFollowedByAccountUpdate_MoreThan90Seconds() {
        for(int i = 0; i < 5; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.CRITICAL,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test " + i,
                    "Failed login attempt for user: test " + i,
                    LocalDateTime.now().minusSeconds((i+1)*30)
                )
            );
        }

        Log log1 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "Successful login attempt for user: test",
            LocalDateTime.now().minusSeconds(10)
        );
        Log log2 = this.logService.save(
            LogType.NOTICE,
            "192.168.1.1",
            "test platform",
            "test software",
            "test",
            "User test updated his/hers account",
            LocalDateTime.now()
        );

        this.ruleService.insertLog(log1);
        this.ruleService.insertLog(log2);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#8");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_09_MultipleThreatsDetectedForTheSameMachine_LessThan7Threats() {
        for (int i = 0; i < 5; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.WARNING,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test",
                    "Virus threat detected",
                    LocalDateTime.now().minusSeconds(45 - i)
                )
            );
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#9");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_09_MultipleThreatsDetectedForTheSameMachine_DifferentIPs() {
        for (int i = 0; i < 9; i++) {
            String ip = "192.168.1.1";
            if (i > 4) {
                ip = "192.168.1.2";
            }
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.WARNING,
                    ip,
                    "test platform",
                    "test software",
                    "test",
                    "Virus threat detected",
                    LocalDateTime.now().minusSeconds(45 - i)
                )
            );
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#9");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_09_MultipleThreatsDetectedForTheSameMachine_MoreThan10Days() {
        for (int i = 0; i < 9; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.WARNING,
                    "192.168.1.93",
                    "test platform",
                    "test software",
                    "test",
                    "Virus threat detected",
                    LocalDateTime.now().minusDays(i+4)
                )
            );
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#9");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_10_LoginAttemptFromMaliciousIPAddress_FailedLoginAttempt() {
        Log log = this.logService.save(
            LogType.CRITICAL,
            "192.168.1.125",
            "test platform",
            "test software",
            "test",
            "Failed login attempt for user: test",
            LocalDateTime.now()
        );

        log.getIp().setMalicious(true);
        this.ipAddressService.save(log.getIp());

        this.ruleService.insertLog(log);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#10");
        assertEquals(1, alarms.size());
    }

    @Test
    public void Rule_10_LoginAttemptFromMaliciousIPAddress_NotMalicious() {
        Log log = this.logService.save(
            LogType.CRITICAL,
            "192.168.1.129",
            "test platform",
            "test software",
            "test",
            "Failed login attempt for user: test",
            LocalDateTime.now()
        );

        this.ruleService.insertLog(log);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#10");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_11_LogFromMaliciousIPAddress_NotMalicious() {
        Log log = this.logService.save(
            LogType.INFORMATION,
            "192.168.1.129",
            "test platform",
            "test software",
            "test",
            "Successful operation",
            LocalDateTime.now()
        );

        this.ruleService.insertLog(log);

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#11");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_12_DOSAttack_LessThan50() {
        for (int i = 0; i < 49; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.WARNING,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test",
                    "Virus threat detected",
                    LocalDateTime.now()
                )
            );
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#12");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_12_DOSAttack_MoreThan60Seconds() {
        for (int i = 0; i < 50; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.WARNING,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test",
                    "Virus threat detected",
                    LocalDateTime.now().minusSeconds(i + 20)
                )
            );
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#12");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_13_PaymentSubsystemAttack_LessThan50() {
        for (int i = 0; i < 49; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.ERROR,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test",
                    "payment transaction failed",
                    LocalDateTime.now()
                )
            );
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#13");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_13_PaymentSubsystemAttack_MoreThan60Seconds() {
        for (int i = 0; i < 50; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.ERROR,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test",
                    "payment transaction failed",
                    LocalDateTime.now().minusSeconds(i + 20)
                )
            );
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#13");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_14_BruteForceAttack_LessThan50() {
        for (int i = 0; i < 49; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.CRITICAL,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test",
                    "Failed login attempt for user: test",
                    LocalDateTime.now()
                )
            );
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#14");
        assertEquals(0, alarms.size());
    }

    @Test
    public void Rule_14_BruteForceAttack_MoreThan60Seconds() {
        for (int i = 0; i < 50; i++) {
            this.ruleService.insertLog(
                this.logService.save(
                    LogType.CRITICAL,
                    "192.168.1.1",
                    "test platform",
                    "test software",
                    "test",
                    "Failed login attempt for user: test",
                    LocalDateTime.now().minusSeconds(i + 10)
                )
            );
        }

        this.ruleService.execute();

        List<Alarm> alarms = this.ruleService.getAlarms("#14");
        assertEquals(1, alarms.size());
    }
}