package com.megatravel.siemcenter;

import com.megatravel.siemcenter.common.util.SearchQueryParser;
import org.apache.xmlbeans.impl.xb.xsdschema.ListDocument;
import org.drools.template.DataProviderCompiler;
import org.drools.template.model.Package;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.KieServices;
import org.kie.api.builder.KieScanner;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.definition.KiePackage;
import org.kie.api.runtime.KieContainer;
import org.kie.internal.utils.KieHelper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.Collection;
import java.util.List;

@SpringBootApplication
public class SiemCenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SiemCenterApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
					.allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH");
			}
		};
	}

	@Bean
	public KieContainer kieContainer() {
		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks
				.newKieContainer(ks.newReleaseId("com.megatravel", "siem-center-rules", "0.0.1-SNAPSHOT"));
		KieScanner kScanner = ks.newKieScanner(kContainer);
		kScanner.start(2_000);
		return kContainer;
	}

	@Bean
	public KieBaseConfiguration baseConfiguration() {
		KieServices ks = KieServices.Factory.get();
		KieBaseConfiguration baseConf = ks.newKieBaseConfiguration();
		baseConf.setOption(EventProcessingOption.STREAM);
		return baseConf;
	}

	@Bean
	public SearchQueryParser queryParser() {
		return new SearchQueryParser();
	}

	@Bean
	public KieHelper kieHelper() {
		return new KieHelper();
	}

	@Bean
	public DataProviderCompiler dataProviderCompiler() {
		return new DataProviderCompiler();
	}
}
