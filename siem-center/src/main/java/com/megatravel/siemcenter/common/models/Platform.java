package com.megatravel.siemcenter.common.models;

import org.springframework.data.mongodb.core.index.Indexed;

import javax.persistence.Id;
import java.util.Objects;

public class Platform implements Comparable<Platform> {
    @Id
    private String id;
    @Indexed(unique = true)
    private String value;

    public Platform() {
    }

    public Platform(String value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int compareTo(Platform platform) {
        return this.value.compareTo(platform.getValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Platform platform = (Platform) o;
        return Objects.equals(value, platform.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
