package com.megatravel.siemcenter.common.models;

public enum RetentionPolicy {
    SHORT, MODERATE, LONG;
}
