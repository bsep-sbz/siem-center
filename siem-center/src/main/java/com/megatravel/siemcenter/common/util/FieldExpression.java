package com.megatravel.siemcenter.common.util;

public class FieldExpression extends Expression {
    private String fieldName;
    private Class fieldType;

    private FieldOperation operation;
    private Object value;

    public FieldExpression() {
    }

    public FieldExpression(String fieldName, FieldOperation operation, Object value) {
        this.fieldName = fieldName;
        this.operation = operation;
        this.value = value;
    }

    public Class getFieldType() {
        return fieldType;
    }

    public void setFieldType(Class fieldType) {
        this.fieldType = fieldType;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public FieldOperation getOperation() {
        return operation;
    }

    public void setOperation(FieldOperation operation) {
        this.operation = operation;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
