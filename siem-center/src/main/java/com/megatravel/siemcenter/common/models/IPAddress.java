package com.megatravel.siemcenter.common.models;

import org.springframework.data.mongodb.core.index.Indexed;

import javax.persistence.Id;
import java.util.Objects;

public class IPAddress implements Comparable<IPAddress> {
    @Id
    private String id;
    @Indexed(unique = true)
    private String value;
    private boolean malicious;

    public IPAddress(String value) {
        super();
        this.value = value;
    }

    public IPAddress() {
        this.malicious = false;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getMalicious() {
        return malicious;
    }

    public void setMalicious(boolean malicious) {
        this.malicious = malicious;
    }

    @Override
    public int compareTo(IPAddress ipAddress) {
        return this.value.compareTo(ipAddress.getValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IPAddress ipAddress = (IPAddress) o;
        return Objects.equals(value, ipAddress.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
