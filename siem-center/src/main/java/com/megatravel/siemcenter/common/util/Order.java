package com.megatravel.siemcenter.common.util;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.data.domain.Sort;
import org.springframework.data.mapping.MappingException;

public enum Order {
    ASC, DSC;

    @JsonCreator
    public static Order forValue(String value) {
        if (value.equalsIgnoreCase("asc")) {
            return ASC;
        }
        if (value.equalsIgnoreCase("dsc")) {
            return DSC;
        }
        throw new MappingException("Sort order value '" + value + "' not supported. Please use 'asc' for ascending order and 'dsc' for descending order.");
    }

    @JsonValue
    public static String toValue(Order order) {
        return order.name().toLowerCase();
    }

    public static Sort.Order by(String property, Order order) {
        if (order.equals(ASC)) {
            return Sort.Order.asc(property);
        } else {
            return Sort.Order.desc(property);
        }
    }
}
