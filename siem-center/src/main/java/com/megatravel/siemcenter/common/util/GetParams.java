package com.megatravel.siemcenter.common.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.megatravel.siemcenter.common.http.exceptions.BadRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mapping.MappingException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GetParams {
    public Integer page;
    public Integer size;
    private String sort;
    private Map<String, Order> _sort;

    private ObjectMapper mapper;

    public GetParams() {
        this.page = 0;
        this.size = 10;
        this.sort = "{}";
        this._sort = new HashMap<>();
        this.mapper = new ObjectMapper();
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) throws BadRequest {
        this.page = page;
        if (this.page < 0) {
            throw new BadRequest("Page must be a positive number.");
        }
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) throws BadRequest {
        this.size = size;
        if (this.size < 0) {
            throw new BadRequest("Size must be a positive number.");
        }
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) throws BadRequest {
        this.sort = sort;

        try {
            Map _sort = mapper.readValue(sort, Map.class);
            for (Object key: _sort.keySet()) {
                if (!(key instanceof String)) {
                    throw new IOException();
                }
                Object value = _sort.get(key);
                if (!(value instanceof String)) {
                    throw new IOException();
                }
                this._sort.put((String)key, Order.forValue((String)value));
            }
        } catch (MappingException|IOException e) {
            throw new BadRequest(e.getMessage());
        }

    }

    public List<Sort.Order> getSortOrder() {
        return this._sort.entrySet()
                .stream()
                .map(entry -> Order.by(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }
}
