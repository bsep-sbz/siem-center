package com.megatravel.siemcenter.common.services;

import com.megatravel.siemcenter.common.models.Platform;
import com.megatravel.siemcenter.common.models.Software;
import com.megatravel.siemcenter.common.repositories.PlatformRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlatformService {
    private final PlatformRepository repository;

    @Autowired
    public PlatformService(PlatformRepository repository) {
        this.repository = repository;
    }

    public Platform save(String value) {
        return this.repository.findByValue(value).orElseGet(
            () -> this.repository.save(new Platform(value))
        );
    }

    public List<Platform> findAll() {
        return this.repository.findAll();
    }
}
