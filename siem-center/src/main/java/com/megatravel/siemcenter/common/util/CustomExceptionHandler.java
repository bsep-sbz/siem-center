package com.megatravel.siemcenter.common.util;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.megatravel.siemcenter.common.http.exceptions.HttpException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(HttpException.class)
    public ResponseEntity<Object> handleHttpException(
        final HttpException ex,
        final WebRequest request
    ) {
        CustomErrorResponse response = new CustomErrorResponse();
        response.path = ((ServletWebRequest)request).getRequest().getRequestURI();
        response.status = HttpStatus.BAD_REQUEST.value();
        response.message = ex.getMessage();

        return handleExceptionInternal(
            ex,
            response,
            new HttpHeaders(),
            ex.getStatus(),
            request
        );
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
        final HttpMessageNotReadableException ex,
        final HttpHeaders headers,
        final HttpStatus status,
        final WebRequest request
    ) {
        CustomErrorResponse response = new CustomErrorResponse();
        response.path = ((ServletWebRequest)request).getRequest().getRequestURI();
        response.status = HttpStatus.BAD_REQUEST.value();

        if (ex.getCause() instanceof JsonMappingException) {
            response.message = ex.getCause()
                .getMessage()
                .split("\\(")[0]
                .trim()
                .replace('"', '\'');
        }

        return handleExceptionInternal(
            ex,
            response,
            new HttpHeaders(),
            HttpStatus.BAD_REQUEST,
            request
        );
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
        final MethodArgumentNotValidException ex,
        final HttpHeaders headers,
        final HttpStatus status,
        final WebRequest request
    ) {

        CustomErrorResponse response = new CustomErrorResponse();
        response.path = ((ServletWebRequest)request).getRequest().getRequestURI();
        response.status = HttpStatus.BAD_REQUEST.value();

        if (ex.getBindingResult().getFieldErrors().size() > 1) {
            Map<String, String> messages = new HashMap<>();
            ex.getBindingResult()
                    .getFieldErrors()
                    .forEach(f -> messages.put(f.getField(), f.getDefaultMessage()));
            response.message = messages;
        } else {
            FieldError error = ex.getBindingResult().getFieldError();
            if (error != null) {
                response.message = error.getField() + ": " + error.getDefaultMessage();
            }
        }

        return handleExceptionInternal(
            ex,
            response,
            headers,
            status,
            request
        );
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        CustomErrorResponse response = new CustomErrorResponse();
        response.path = ((ServletWebRequest)request).getRequest().getRequestURI();
        response.status = HttpStatus.BAD_REQUEST.value();
        List<FieldError> results = ex.getBindingResult().getFieldErrors();

        if (results.size() > 1) {
            Map<String, String> errors = new HashMap<>();
            String message;
            int index;
            for (final FieldError error : results) {
                message = error.getDefaultMessage();
                if (message != null) {
                    index = message.indexOf(":") + 2;
                    errors.put(error.getField(), message.substring(index));
                }
            }
            response.message = errors;
        } else {
            FieldError error = results.get(0);

            String message = error.getDefaultMessage();
            if (message != null) {
                int index = message.indexOf(":") + 2;
                response.message = message.substring(index);
            }

        }

        return handleExceptionInternal(
            ex,
            response,
            headers,
            status,
            request
        );
    }
}
