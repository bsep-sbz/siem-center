package com.megatravel.siemcenter.common.services;

import com.megatravel.siemcenter.common.models.IPAddress;
import com.megatravel.siemcenter.common.repositories.IPAddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IPAddressService {
    private final IPAddressRepository repository;

    @Autowired
    public IPAddressService(IPAddressRepository repository) {
        this.repository = repository;
    }

    public IPAddress save(String value) {
        return this.repository.findByValue(value).orElseGet(
            () -> this.repository.save(new IPAddress(value))
        );
    }

    public IPAddress save(IPAddress ipAddress) {
        return this.repository.save(ipAddress);
    }

    public List<IPAddress> findAll() {
        return this.repository.findAll();
    }

    public IPAddress findByValue(String value) {
        return this.repository.findByValue(value).get();
    }
}
