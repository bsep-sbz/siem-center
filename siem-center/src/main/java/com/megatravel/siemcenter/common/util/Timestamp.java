package com.megatravel.siemcenter.common.util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.sql.Time;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

public class Timestamp implements Comparable {
    @JsonValue
    private LocalDateTime value;

    public Timestamp(LocalDateTime value) {
        this.value = value;
    }

    public Timestamp(String value) {
        this.value = Timestamp.parse(value).getValue();
    }

    public Timestamp() {

    }

    @JsonCreator
    public Timestamp(Date value) {
        this.value = value.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public Date toDate() {
        return Date.from(this.value.atZone(ZoneOffset.systemDefault()).toInstant());
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof LocalDateTime) {
            return this.value.compareTo((LocalDateTime) o);
        }

        if (o instanceof String) {
            Instant instant = Instant.parse((String) o);
            LocalDateTime timestamp = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
            return this.value.compareTo(timestamp);
        }

        return -1;
    }

    public static Timestamp parse(String value) {
        Instant instant = Instant.parse(value);
        LocalDateTime timestamp = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
        return new Timestamp(timestamp);
    }

    public LocalDateTime getValue() {
        return value;
    }

    public void setValue(LocalDateTime value) {
        this.value = value;
    }
}
