package com.megatravel.siemcenter.common.http.exceptions;

import org.springframework.http.HttpStatus;

public class NotFound extends HttpException {

    public NotFound(String message) {
        super(message);
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.NOT_FOUND;
    }
}
