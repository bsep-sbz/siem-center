package com.megatravel.siemcenter.common.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.*;

// TODO: reevaluate exceptions

public class SearchQueryParser {

    private ObjectMapper mapper = new ObjectMapper();
    private Class clazz;

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public SearchQuery parse(String rawQuery) throws IOException {
        Map<String, Object> expression = mapper.readValue(rawQuery, new TypeReference<Map<String, Object>>() {});
        return this.parse(expression);
    }

    public SearchQuery parse(Map<String, Object> expression) {
        SearchQuery query = new SearchQuery();

        if (expression.keySet().size() == 1) {
            query.setExpression(parseLogicalExpression(expression));
        } else if(expression.keySet().size() == 3) {
            query.setExpression(parseFieldExpression(expression));
        } else {
            throw new IllegalArgumentException("Invalid search query.");
        }

        return query;
    }

    private LogicalExpression parseLogicalExpression(Map<String, Object> expression) {
        String operation = expression.keySet().iterator().next();

        LogicalOperation op;
        try {
            op = LogicalOperation.valueOf(operation.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("JSON field name '" + operation + "' is not a valid logical " +
                    "operation. Available logical operations are 'all', 'any' and 'not'.");
        }

        Object value = expression.get(operation);

        LogicalExpression exp = new LogicalExpression(op);

        if (op.equals(LogicalOperation.NOT)) {
            try {
                @SuppressWarnings("unchecked")
                Map<String, Object> val = (Map<String, Object>)value;
                exp.getParams().add(parseFieldExpression(val));
            } catch (ClassCastException e) {
                throw new IllegalArgumentException("Not operation is unary and accepts a JSON object.");
            }

        } else {
            if (!(value instanceof List)) {
                throw new IllegalArgumentException("All and any operations accept a collection of JSON objects.");
            }

            List val = (ArrayList) value;

            for(Object element: val) {
                try {
                    @SuppressWarnings("unchecked")
                    Map<String, Object> el = (Map<String, Object>)element;

                    if (el.keySet().size() == 1) {
                        exp.getParams().add(parseLogicalExpression(el));
                    } else if(el.keySet().size() == 3) {
                        exp.getParams().add(parseFieldExpression(el));
                    } else {
                        throw new IllegalArgumentException("Unrecognized expression in '" + operation + "' operation.");
                    }

                } catch (ClassCastException e) {
                    throw new IllegalArgumentException("All and any operations accept a collection of JSON objects.");
                }
            }
        }

        return exp;
    }

    private FieldExpression parseFieldExpression(Map<String, Object> expression) {
        FieldExpression exp = new FieldExpression();

        for(String key: new String[]{"field", "operation", "value"}) {
            if (!expression.containsKey(key)) {
                throw new IllegalArgumentException("Field expression must contain 'field', 'operation' and 'value' fields.");
            }
        }

        Map<String, Class> fields = new HashMap<>();
        Arrays.stream(this.clazz.getDeclaredFields()).forEach(field -> {
            fields.put(field.getName(), field.getType());
        });

        String field = (String) expression.get("field");
        if (!fields.containsKey(field)) {
            throw new IllegalArgumentException("Field name '" + field + "' is not valid.");
        }
        exp.setFieldName(field);

        if (!(expression.get("operation") instanceof String)) {
            throw new IllegalArgumentException("");
        }
        String op = (String) expression.get("operation");
        exp.setOperation(FieldOperation.valueOf(op.toUpperCase()));

        Class type = fields.get(field);
        if (!type.getName().equalsIgnoreCase("string") && op.equalsIgnoreCase("regex")) {
            throw new IllegalArgumentException("Regex operation is only valid for string fields.");
        }

        exp.setValue(expression.get("value"));

        return exp;
    }
}
