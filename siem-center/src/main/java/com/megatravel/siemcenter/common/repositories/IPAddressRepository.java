package com.megatravel.siemcenter.common.repositories;

import com.megatravel.siemcenter.common.models.IPAddress;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IPAddressRepository extends MongoRepository<IPAddress, String> {
    Optional<IPAddress> findByValue(String value);
}
