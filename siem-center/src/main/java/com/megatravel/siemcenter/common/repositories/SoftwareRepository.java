package com.megatravel.siemcenter.common.repositories;

import com.megatravel.siemcenter.common.models.Software;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SoftwareRepository extends MongoRepository<Software, String> {
    Optional<Software> findByValue(String value);
}
