package com.megatravel.siemcenter.common.repositories;

import com.megatravel.siemcenter.common.models.Platform;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlatformRepository extends MongoRepository<Platform, String> {
    Optional<Platform> findByValue(String value);
}
