package com.megatravel.siemcenter.common.util;

import java.time.LocalDateTime;

public class CustomErrorResponse {
    public LocalDateTime timestamp;
    public Object message;
    public String path;
    public int status;

    public CustomErrorResponse() {
        this.message = null;
        this.timestamp = LocalDateTime.now();
    }

    public CustomErrorResponse(Object message, String path, int status) {
        this();
        this.message = message;
        this.path = path;
        this.status = status;
    }
}
