package com.megatravel.siemcenter.common.models;

import com.fasterxml.jackson.annotation.JsonValue;

public enum RiskFactor {
    LOW, MODERATE, HIGH, EXTREME;

    @JsonValue
    public String toJSON() {
        return this.name().toLowerCase();
    }
}
