package com.megatravel.siemcenter.common.util;

public class SearchQuery {
    private Expression expression;

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }
}
