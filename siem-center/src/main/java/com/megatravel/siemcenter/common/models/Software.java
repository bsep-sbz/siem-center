package com.megatravel.siemcenter.common.models;

import org.springframework.data.mongodb.core.index.Indexed;

import javax.persistence.Id;
import java.util.Objects;

public class Software implements Comparable<Software> {
    @Id
    private String id;
    @Indexed(unique = true)
    private String value;

    public Software() {
    }

    public Software(String value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int compareTo(Software software) {
        return this.value.compareTo(software.getValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Software software = (Software) o;
        return Objects.equals(value, software.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
