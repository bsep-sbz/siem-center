package com.megatravel.siemcenter.common.http.exceptions;

import org.springframework.http.HttpStatus;

public abstract class HttpException extends Exception {

    protected HttpException(String message) {
        super(message);
    }

    public abstract HttpStatus getStatus();

}
