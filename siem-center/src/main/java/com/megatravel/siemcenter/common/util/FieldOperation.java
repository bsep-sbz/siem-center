package com.megatravel.siemcenter.common.util;

public enum FieldOperation {
    NEQ, LT, LE, EQ, GE, GT, REGEX;
}
