package com.megatravel.siemcenter.common.services;

import com.megatravel.siemcenter.common.models.Software;
import com.megatravel.siemcenter.common.repositories.SoftwareRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SoftwareService {
    private final SoftwareRepository repository;

    @Autowired
    public SoftwareService(SoftwareRepository repository) {
        this.repository = repository;
    }

    public Software save(String value) {
        return this.repository.findByValue(value).orElseGet(
            () -> this.repository.save(new Software(value))
        );
    }

    public List<Software> findAll() {
        return this.repository.findAll();
    }
}
