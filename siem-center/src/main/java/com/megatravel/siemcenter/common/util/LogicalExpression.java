package com.megatravel.siemcenter.common.util;

import java.util.LinkedList;
import java.util.List;

public class LogicalExpression extends Expression {
    private LogicalOperation operation;
    private List<Expression> params;

    public LogicalExpression() {
        params = new LinkedList<>();
    }

    public LogicalExpression(LogicalOperation operation) {
        this();
        this.operation = operation;
    }

    public LogicalOperation getOperation() {
        return operation;
    }

    public void setOperation(LogicalOperation operation) {
        this.operation = operation;
    }

    public List<Expression> getParams() {
        return params;
    }

    public void setParams(List<Expression> params) {
        this.params = params;
    }
}
