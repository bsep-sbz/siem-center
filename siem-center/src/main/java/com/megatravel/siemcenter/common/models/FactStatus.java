package com.megatravel.siemcenter.common.models;

import com.fasterxml.jackson.annotation.JsonValue;

public enum FactStatus {
    PENDING, ACTIVE, ARCHIVED;

    @JsonValue
    public String toJSON() {
        return this.name().toLowerCase();
    }
}
