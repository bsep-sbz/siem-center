package com.megatravel.siemcenter.common.util;

public enum LogicalOperation {
    OR, AND, NOT;
}
