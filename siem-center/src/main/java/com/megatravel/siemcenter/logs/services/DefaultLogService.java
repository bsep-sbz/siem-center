package com.megatravel.siemcenter.logs.services;

import com.megatravel.siemcenter.common.models.IPAddress;
import com.megatravel.siemcenter.common.models.Software;
import com.megatravel.siemcenter.common.services.IPAddressService;
import com.megatravel.siemcenter.common.services.PlatformService;
import com.megatravel.siemcenter.common.services.SoftwareService;
import com.megatravel.siemcenter.common.util.Timestamp;
import com.megatravel.siemcenter.logs.models.Log;
import com.megatravel.siemcenter.common.models.FactStatus;
import com.megatravel.siemcenter.logs.models.LogType;
import com.megatravel.siemcenter.logs.repositories.LogRepository;
import com.megatravel.siemcenter.users.services.UserService;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class DefaultLogService implements LogService {
    private final LogRepository repository;
    private final IPAddressService ipAddressService;
    private final PlatformService platformService;
    private final SoftwareService softwareService;
    private final UserService userService;

    @Autowired
    public DefaultLogService(LogRepository repository, IPAddressService ipAddressService, PlatformService platformService, SoftwareService softwareService, UserService userService) {
        this.repository = repository;
        this.ipAddressService = ipAddressService;
        this.platformService = platformService;
        this.softwareService = softwareService;
        this.userService = userService;
    }

    @Override
    public Log save(LogType type, String ip, String platform, String software, String username, String message, LocalDateTime timestamp) {
        Log log = new Log();

        log.setType(type);
        log.setIp(this.ipAddressService.save(ip));
        log.setPlatform(this.platformService.save(platform));
        log.setSoftware(this.softwareService.save(software));
        log.setMessage(message);
        log.setTimestamp(new Timestamp(timestamp));
        log.setUser(this.userService.save(username));

        log.setStatus(FactStatus.PENDING);

        return this.repository.save(log);
    }

    @Override
    public void update(Log log) {
        this.repository.save(log);
    }

    @Override
    public Optional<Log> findOne(String id) {
        return this.repository.findById(id);
    }

    @Override
    public List<Log> findAll() {
        return this.repository.findAll();
    }

    @Override
    public List<Log> findAllActive() {
        return this.repository.findAllByStatus(FactStatus.ACTIVE);
    }

    @Override
    public void remove(String id) {
        this.repository.deleteById(id);
    }

    @Override
    public Log updateStatus(String id, FactStatus status) {
        Optional<Log> log = this.repository.findById(id);
        if (log.isPresent()) {
            Log l = log.get();
            l.setStatus(status);
            return this.repository.save(l);
        }
        return null;
    }

    @Override
    public int numberOfLogsPerIp(IPAddress ipAddress, LocalDateTime start_time, LocalDateTime end_time) {
        int counter = 0;
        LocalDateTime timestamp;
        for (Log log: this.repository.findAllByIp(ipAddress)) {
            timestamp = log.getTimestamp().getValue();
            if (timestamp.isAfter(start_time) && timestamp.isBefore(end_time)) {
                counter += 1;
            }
        }
        return counter;
    }

    @Override
    public int numberOfLogsPerSoftware(Software software, LocalDateTime start_time, LocalDateTime end_time) {
        int counter = 0;
        LocalDateTime timestamp;
        for (Log log: this.repository.findAllBySoftware(software)) {
            timestamp = log.getTimestamp().getValue();
            if (timestamp.isAfter(start_time) && timestamp.isBefore(end_time)) {
                counter += 1;
            }
        }
        return counter;
    }


}
