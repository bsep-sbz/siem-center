package com.megatravel.siemcenter.logs.models;

import com.fasterxml.jackson.annotation.*;
import com.megatravel.siemcenter.common.models.*;
import com.megatravel.siemcenter.common.util.Timestamp;
import com.megatravel.siemcenter.users.models.User;
import org.drools.core.factmodel.traits.Traitable;
import org.kie.api.definition.type.Role;
import org.springframework.data.mongodb.core.mapping.DBRef;

import javax.persistence.Id;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


@org.kie.api.definition.type.Role(Role.Type.EVENT)
@org.kie.api.definition.type.Timestamp("_timestamp")
@Traitable
public class Log implements Comparable<Log> {
    @Id
    private String id;

    private String message;
    private LogType type;
    private Timestamp timestamp;

    @DBRef
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "value")
    @JsonIdentityReference(alwaysAsId = true)
    private IPAddress ip;

    @DBRef
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "value")
    @JsonIdentityReference(alwaysAsId = true)
    private Platform platform;

    @DBRef
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "value")
    @JsonIdentityReference(alwaysAsId = true)
    private Software software;

    @DBRef
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "username")
    @JsonIdentityReference(alwaysAsId = true)
    private User user;

    @JsonIgnore
    private Date _timestamp;

    @JsonIgnore
    private FactStatus status;

    @JsonIgnore
    private Map<String, Object> metadata;

    public Log() {
        this.metadata = new HashMap<>();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LogType getType() {
        return type;
    }

    public void setType(LogType type) {
        this.type = type;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
        this._timestamp = this.timestamp.toDate();
    }

    public IPAddress getIp() {
        return ip;
    }

    public void setIp(IPAddress ip) {
        this.ip = ip;
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public Software getSoftware() {
        return software;
    }

    public void setSoftware(Software software) {
        this.software = software;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date get_timestamp() {
        return _timestamp;
    }

    public FactStatus getStatus() {
        return status;
    }

    public void setStatus(FactStatus status) {
        this.status = status;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    @Override
    public int compareTo(Log log) {
        return this.timestamp.compareTo(log.getTimestamp());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Log log = (Log) o;
        return Objects.equals(id, log.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
