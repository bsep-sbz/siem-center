package com.megatravel.siemcenter.logs.controllers;

import com.megatravel.siemcenter.common.util.SearchQuery;
import com.megatravel.siemcenter.common.util.SearchQueryParser;
import com.megatravel.siemcenter.logs.models.Log;
import com.megatravel.siemcenter.logs.requests.SaveLogRequest;
import com.megatravel.siemcenter.logs.services.LogService;
import com.megatravel.siemcenter.rules.services.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path="/logs")
public class LogController {
    private final LogService logService;
    private final RuleService ruleService;

    private final SearchQueryParser queryParser;

    @Autowired
    public LogController(LogService logService, RuleService ruleService, SearchQueryParser queryParser) {
        this.logService = logService;
        this.ruleService = ruleService;
        this.queryParser = queryParser;
        this.queryParser.setClazz(Log.class);
    }

    @PostMapping
    public Log saveLog(@Valid @RequestBody SaveLogRequest data, HttpServletRequest request) {
        data.ip = request.getRemoteAddr();
        Log log = this.logService.save(
            data.type,
            data.ip,
            data.platform,
            data.software,
            data.user,
            data.message,
            data.timestamp
        );
        this.ruleService.insertLog(log);
        this.ruleService.execute();
        return log;
    }

    @GetMapping
    public List<Log> findAllLogs() {
        return this.logService.findAll();
    }

    @PostMapping("/search")
    public List<Log> searchLogs(@RequestBody Map<String, Object> query) {
        SearchQuery searchQuery = this.queryParser.parse(query);
        return this.logService.findAll();
    }
}
