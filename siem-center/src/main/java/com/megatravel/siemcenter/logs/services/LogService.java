package com.megatravel.siemcenter.logs.services;

import com.megatravel.siemcenter.common.models.IPAddress;
import com.megatravel.siemcenter.common.models.Software;
import com.megatravel.siemcenter.logs.models.Log;
import com.megatravel.siemcenter.common.models.FactStatus;
import com.megatravel.siemcenter.logs.models.LogType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import javax.persistence.criteria.CriteriaBuilder;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface LogService {

    Log save(LogType type, String ip, String platform, String software, String username, String message, LocalDateTime timestamp);
    void update(Log log);
    Optional<Log> findOne(String id);
    List<Log> findAll();
    List<Log> findAllActive();
    void remove(String id);
    Log updateStatus(String id, FactStatus status);
    int numberOfLogsPerIp(IPAddress ipAddress, LocalDateTime start_time, LocalDateTime end_time);
    int numberOfLogsPerSoftware(Software software, LocalDateTime start_time, LocalDateTime end_time);
}
