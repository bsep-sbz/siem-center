package com.megatravel.siemcenter.logs.requests;

import com.megatravel.siemcenter.logs.models.LogType;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;

public class SaveLogRequest {
    @NotNull(message = "invalid or not provided value; available types are: crit, err, warn, notice and info")
    public LogType type;
    @Null
    public String ip;
    public String platform = "";
    public String software = "";
    @NotBlank
    public String user;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    public LocalDateTime timestamp;
    public String message = "";

    public SaveLogRequest() {}
}
