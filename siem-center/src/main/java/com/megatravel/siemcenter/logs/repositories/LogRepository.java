package com.megatravel.siemcenter.logs.repositories;

import com.megatravel.siemcenter.common.models.FactStatus;
import com.megatravel.siemcenter.common.models.IPAddress;
import com.megatravel.siemcenter.common.models.Platform;
import com.megatravel.siemcenter.common.models.Software;
import com.megatravel.siemcenter.logs.models.Log;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface LogRepository extends MongoRepository<Log, String> {

    List<Log> findAllByStatus(FactStatus status);
    List<Log> findAllByIp(IPAddress ipAddress);
    List<Log> findAllBySoftware(Software software);
}
