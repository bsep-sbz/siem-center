package com.megatravel.siemcenter.logs.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.StringUtils;


import java.util.HashMap;
import java.util.Map;

public enum LogType {
    CRITICAL, ERROR, WARNING, NOTICE, INFORMATION;

    private static Map<String, LogType> namesMap = new HashMap<String, LogType>(5);

    static {
        namesMap.put("crit", CRITICAL);
        namesMap.put("err", ERROR);
        namesMap.put("warn", WARNING);
        namesMap.put("notice", NOTICE);
        namesMap.put("info", INFORMATION);
    }

    @JsonCreator
    public static LogType forValue(String value) {
        return namesMap.get(StringUtils.lowerCase(value));
    }

    @JsonValue
    public String toValue() {
        for (Map.Entry<String, LogType> entry : namesMap.entrySet()) {
            if (entry.getValue() == this)
                return entry.getKey();
        }

        return null;
    }
}
