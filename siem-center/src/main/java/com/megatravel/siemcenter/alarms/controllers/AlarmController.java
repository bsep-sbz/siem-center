package com.megatravel.siemcenter.alarms.controllers;

import com.megatravel.siemcenter.alarms.models.Alarm;
import com.megatravel.siemcenter.alarms.services.AlarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/alarms")
public class AlarmController {

    private final AlarmService alarmService;

    @Autowired
    public AlarmController(AlarmService alarmService) {
        this.alarmService = alarmService;
    }

    @GetMapping
    public List<Alarm> findAll() {
        return this.alarmService.findAll();
    }
}
