package com.megatravel.siemcenter.alarms.services;

import com.megatravel.siemcenter.alarms.models.Alarm;
import com.megatravel.siemcenter.alarms.repositories.AlarmRepository;
import com.megatravel.siemcenter.common.models.IPAddress;
import com.megatravel.siemcenter.common.models.Software;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultAlarmService implements AlarmService {

    private final AlarmRepository repository;

    @Autowired
    public DefaultAlarmService(AlarmRepository repository) {
        this.repository = repository;
    }

    @Override
    public Alarm save(Alarm alarm) {
        return this.repository.save(alarm);
    }

    @Override
    public List<Alarm> findAll() {
        return this.repository.findAll();
    }
}
