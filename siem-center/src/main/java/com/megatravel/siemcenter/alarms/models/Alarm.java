package com.megatravel.siemcenter.alarms.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.megatravel.siemcenter.common.models.FactStatus;
import com.megatravel.siemcenter.common.util.Timestamp;
import com.megatravel.siemcenter.logs.models.Log;
import com.megatravel.siemcenter.users.models.User;
import org.drools.core.factmodel.traits.Traitable;
import org.kie.api.definition.type.Role;

import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.*;

@org.kie.api.definition.type.Role(Role.Type.EVENT)
@org.kie.api.definition.type.Timestamp("_timestamp")
@Traitable
public class Alarm {
    @Id
    private String id;

    private String message;
    private Timestamp timestamp;
    private List<Log> associatedLogs;

    @JsonIgnore
    private Date _timestamp;
    @JsonIgnore
    private Set<User> associatedUsers;
    @JsonIgnore
    private FactStatus status;
    @JsonIgnore
    private String forRule;

    public Alarm() {
        this.associatedLogs = new LinkedList<>();
        this.associatedUsers = new HashSet<>();
        this.timestamp = new Timestamp(LocalDateTime.now());
        this._timestamp = this.timestamp.toDate();
    }

    public Alarm(String message) {
        super();
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
        this._timestamp = timestamp.toDate();
    }

    public List<Log> getAssociatedLogs() {
        return associatedLogs;
    }

    public void setAssociatedLogs(List<Log> associatedLogs) {
        this.associatedLogs = associatedLogs;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date get_timestamp() {
        return _timestamp;
    }

    public void set_timestamp(Date _timestamp) {
        this._timestamp = _timestamp;
    }

    public FactStatus getStatus() {
        return status;
    }

    public void setStatus(FactStatus status) {
        this.status = status;
    }

    public String getForRule() {
        return forRule;
    }

    public void setForRule(String forRule) {
        this.forRule = forRule;
    }

    public Set<User> getAssociatedUsers() {
        return associatedUsers;
    }

    public void setAssociatedUsers(Set<User> associatedUsers) {
        this.associatedUsers = associatedUsers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Alarm alarm = (Alarm) o;
        return Objects.equals(id, alarm.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
