package com.megatravel.siemcenter.alarms.services;

import com.megatravel.siemcenter.alarms.models.Alarm;
import com.megatravel.siemcenter.common.models.IPAddress;
import com.megatravel.siemcenter.common.models.Software;

import java.util.List;

public interface AlarmService {
    Alarm save(Alarm alarm);
    List<Alarm> findAll();
}
