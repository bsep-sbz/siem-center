package com.megatravel.siemcenter.alarms.repositories;

import com.megatravel.siemcenter.alarms.models.Alarm;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AlarmRepository extends MongoRepository<Alarm, String> {
}
