package com.megatravel.siemcenter.rules.exceptions;

public class RuleInterpretationException extends Exception {

    public RuleInterpretationException(String s) {
        super(s);
    }
}
