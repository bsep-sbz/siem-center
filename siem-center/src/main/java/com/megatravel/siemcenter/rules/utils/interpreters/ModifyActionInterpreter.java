package com.megatravel.siemcenter.rules.utils.interpreters;

import java.util.Map;

public class ModifyActionInterpreter implements Interpreter<Map<String, Object>> {
    @Override
    public Map<String, Object> process(Object input) {
        if (!this.validate(input)) {
            return null;
        }

        @SuppressWarnings("unchecked")
        Map<String, Object> obj = (Map<String, Object>) input;
        return obj;
    }

    @Override
    public boolean validate(Object input) {
        return false;
    }
}
