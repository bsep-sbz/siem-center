package com.megatravel.siemcenter.rules.utils.interpreters;

import com.megatravel.siemcenter.rules.models.Frame;
import com.megatravel.siemcenter.rules.models.FrameType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class FrameInterpreter implements Interpreter<Frame> {

    private final InterpretationErrors errors;

    @Autowired
    public FrameInterpreter(InterpretationErrors errors) {
        this.errors = errors;
    }

    @Override
    public Frame process(Object input) {
        if(!this.validate(input)) {
            return null;
        }

        @SuppressWarnings("unchecked")
        Map obj = (Map<String, Object>) input;

        Frame frame = new Frame();

        String type = (String)obj.get("type");
        frame.setType(FrameType.valueOf(type.toUpperCase()));

        frame.setValue((String) obj.get("value"));

        return frame;
    }

    @Override
    public boolean validate(Object input) {
        if (!(input instanceof Map)) {
            errors.add("Frame must be a JSON object.");
            return false;
        }

        @SuppressWarnings("unchecked")
        Map obj = (Map<String, Object>) input;

        boolean valid = true;
        for(String key: new String[] { "type", "value"}) {
            if (!obj.containsKey(key)) {
                errors.add("'Fact' object is missing field '" + key + "'.");
                valid = false;
            }
        }

        if (!(obj.get("type") instanceof String)) {
            errors.add("Type field in 'Frame' object must be a text value.");
            valid = false;
        } else {
            String type = (String)obj.get("type");
            try {
                FrameType.valueOf(type.toUpperCase());
            } catch (IllegalArgumentException e) {
                errors.add("Frame type '" + type + "' is invalid. Available frame types are 'length' and 'time'.");
                valid = false;
            }
        }

        if (!(obj.get("value") instanceof String)) {
            errors.add("Value field in 'Frame' object must be a text value.");
            valid = false;
        }

        return valid;
    }
}
