package com.megatravel.siemcenter.rules.services;

import com.megatravel.siemcenter.alarms.models.Alarm;
import com.megatravel.siemcenter.logs.models.Log;
import com.megatravel.siemcenter.rules.exceptions.RuleInterpretationException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface RuleService {
    void createRule(Map<String, Object> data) throws RuleInterpretationException, IOException;
    void insertLog(Log log);
    void execute();
    void clearFacts();

    List<Log> getLogs();
    List<Alarm> getAlarms();
    List<Alarm> getAlarms(String forRule);
}
