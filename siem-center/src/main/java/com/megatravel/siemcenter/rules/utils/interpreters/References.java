package com.megatravel.siemcenter.rules.utils.interpreters;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashMap;
import java.util.Map;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class References {

    private Map<String, Class> refs;

    public References() {
        this.refs = new HashMap<>();
    }

    public void add(String ref, Class c) {
        if (this.has(ref)) {
            return;
        }
        if (!ref.startsWith("$")) {
            ref = "$" + ref;
        }
        this.refs.put(ref, c);
    }

    public boolean has(String ref) {
        return this.refs.containsKey(ref);
    }

    public Class get(String ref) {
        return this.refs.getOrDefault(ref, null);
    }
}
