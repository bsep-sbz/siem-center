package com.megatravel.siemcenter.rules.utils.generators;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.util.LinkedList;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Imports extends LinkedList<String> {

    @Override
    public boolean add(String s) {
        if (this.contains(s)) {
            return false;
        }
        return super.add(s);
    }
}
