package com.megatravel.siemcenter.rules.utils.interpreters;

import com.megatravel.siemcenter.alarms.models.Alarm;
import com.megatravel.siemcenter.common.models.IPAddress;
import com.megatravel.siemcenter.common.models.Platform;
import com.megatravel.siemcenter.common.models.Software;
import com.megatravel.siemcenter.logs.models.Log;
import com.megatravel.siemcenter.users.models.User;

import java.util.HashMap;
import java.util.Map;

public class Entities {

    private static final Map<String, Class> entities;
    public static Class activeEntity;

    static {
        entities = new HashMap<>();
        entities.put("Log", Log.class);
        entities.put("IPAddress", IPAddress.class);
        entities.put("Platform", Platform.class);
        entities.put("Software", Software.class);
        entities.put("User", User.class);
        entities.put("Alarm", Alarm.class);
    }

    public static Class get(String entity) {
        return entities.get(entity);
    }

    public static boolean has(String entity) {
        return entities.containsKey(entity);
    }
}
