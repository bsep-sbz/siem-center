package com.megatravel.siemcenter.rules.utils.interpreters;

import com.megatravel.siemcenter.common.util.Expression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ExpressionInterpreter implements Interpreter<Expression> {

    private final InterpretationErrors errors;
    private final LogicalExpressionInterpreter logicalExpressionCompiler;
    private final FieldExpressionInterpreter fieldExpressionCompiler;

    @Autowired
    public ExpressionInterpreter(InterpretationErrors errors, @Lazy LogicalExpressionInterpreter logicalExpressionCompiler, @Lazy FieldExpressionInterpreter fieldExpressionCompiler) {
        this.errors = errors;
        this.logicalExpressionCompiler = logicalExpressionCompiler;
        this.fieldExpressionCompiler = fieldExpressionCompiler;
    }

    @Override
    public Expression process(Object input) {
        if (!this.validate(input)) {
            return null;
        }

        Map obj = (Map) input;

        if (obj.keySet().size() == 1) {
            return this.logicalExpressionCompiler.process(obj);
        } else {
            return this.fieldExpressionCompiler.process(obj);
        }
    }

    public boolean validate(Object input) {
        if (!(input instanceof Map)) {
            errors.add("Expression must be a JSON object.");
            return false;
        }

        Map obj = (Map) input;

        if (obj.keySet().size() != 1 && obj.keySet().size() != 3) {
            errors.add("Not a valid logical nor field expression.");
            return false;
        }

        return true;
    }
}
