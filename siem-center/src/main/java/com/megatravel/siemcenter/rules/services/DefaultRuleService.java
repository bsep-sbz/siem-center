package com.megatravel.siemcenter.rules.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.megatravel.siemcenter.alarms.models.Alarm;
import com.megatravel.siemcenter.alarms.services.AlarmService;
import com.megatravel.siemcenter.common.models.FactStatus;
import com.megatravel.siemcenter.common.services.IPAddressService;
import com.megatravel.siemcenter.common.services.LoggingService;
import com.megatravel.siemcenter.common.services.PlatformService;
import com.megatravel.siemcenter.common.services.SoftwareService;
import com.megatravel.siemcenter.logs.models.Log;
import com.megatravel.siemcenter.logs.services.LogService;
import com.megatravel.siemcenter.rules.exceptions.RuleInterpretationException;
import com.megatravel.siemcenter.rules.models.Rule;
import com.megatravel.siemcenter.rules.repositories.RuleRepository;
import com.megatravel.siemcenter.rules.utils.interpreters.InterpretationErrors;
import com.megatravel.siemcenter.rules.utils.interpreters.RuleInterpreter;
import com.megatravel.siemcenter.rules.utils.generators.RuleGenerator;
import com.megatravel.siemcenter.users.services.UserService;
import org.drools.template.DataProvider;
import org.drools.template.DataProviderCompiler;
import org.drools.template.objects.ArrayDataProvider;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.definition.KiePackage;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;
import org.kie.internal.utils.KieHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Service;


@Service
public class DefaultRuleService implements RuleService {

    private static final String GENERIC_TEMPLATE = "/GenericTemplate.drt";

    private final RuleRepository repository;

    private final LogService logService;
    private final AlarmService alarmService;
    private final IPAddressService ipAddressService;
    private final PlatformService platformService;
    private final SoftwareService softwareService;
    private final UserService userService;

    private final RuleInterpreter interpreter;
    private final RuleGenerator generator;
    private final InterpretationErrors errors;

    private final DataProviderCompiler compiler;
    private final KieHelper kieHelper;
    private final KieBaseConfiguration baseConfiguration;
    private final KieContainer kieContainer;
    private KieSession session;

    @Autowired
    public DefaultRuleService(
        RuleRepository repository,
        LogService logService,
        AlarmService alarmService,
        IPAddressService ipAddressService,
        PlatformService platformService,
        SoftwareService softwareService,
        UserService userService,

        RuleInterpreter interpreter,
        RuleGenerator generator,
        InterpretationErrors errors,

        DataProviderCompiler compiler,
        KieHelper kieHelper,
        KieContainer kieContainer,
        KieBaseConfiguration baseConfiguration
    ) throws IOException {
        this.repository = repository;

        this.logService = logService;
        this.alarmService = alarmService;
        this.ipAddressService = ipAddressService;
        this.platformService = platformService;
        this.softwareService = softwareService;
        this.userService = userService;

        this.interpreter = interpreter;
        this.generator = generator;
        this.errors = errors;
        this.compiler = compiler;
        this.kieHelper = kieHelper;
        this.kieContainer = kieContainer;
        this.baseConfiguration = baseConfiguration;

        this.loadStaticRules();
        this.loadDynamicRules();

        this.updateSession();
    }

    @Override
    public void createRule(Map<String, Object> data) throws RuleInterpretationException, IOException {
        Rule rule = this.interpreter.process(data);

        if (!this.errors.isEmpty()) {
            String message = String.join(",\n", this.errors);
            throw new RuleInterpretationException(message);
        }

        String[] result = this.generator.generate(rule);

        DataProvider provider = new ArrayDataProvider(new String[][] { result });
        InputStream template = DefaultRuleService.class.getResourceAsStream(GENERIC_TEMPLATE);
        String drl = this.compiler.compile(provider, template);
        template.close();

        this.kieHelper.addContent(drl, ResourceType.DRL);

        Results results = this.kieHelper.verify();
        if (results.hasMessages(Message.Level.ERROR, Message.Level.WARNING)) {
            String message = results.getMessages()
                .stream()
                .map(Message::getText)
                .collect(Collectors.joining("\n"));
            throw new RuleInterpretationException(message);
        }

        rule.setContent(drl);
        this.repository.save(rule);

        this.updateSession();
    }

    @Override
    public void insertLog(Log log) {
        this.session.insert(log);
        log.setStatus(FactStatus.ACTIVE);
        this.logService.update(log);
    }

    @Override
    public void execute() {
        this.session.fireAllRules();
    }

    @Override
    public void clearFacts() {
        this.session.dispose();
        this.updateSession();
    }

    public List<Log> getLogs() {
        List<Log> logs = new LinkedList<>();
        QueryResults results = this.session.getQueryResults( "getLogs" );
        for ( QueryResultsRow row : results ) {
            logs.add((Log) row.get( "$result" ));
        }
        return logs;
    }

    public List<Alarm> getAlarms() {
        List<Alarm> alarms = new LinkedList<>();
        QueryResults results = this.session.getQueryResults( "getAlarms" );
        for ( QueryResultsRow row : results ) {
            alarms.add((Alarm) row.get( "$result" ));
        }
        return alarms;
    }

    public List<Alarm> getAlarms(String ruleId) {
        List<Alarm> alarms = new LinkedList<>();
        QueryResults results = this.session.getQueryResults( "getAlarmsForRule", ruleId);
        for ( QueryResultsRow row : results ) {
            alarms.add((Alarm) row.get( "$result" ));
        }
        return alarms;
    }

    private void loadDynamicRules() {
        for (Rule rule: this.repository.findAll()) {
            this.kieHelper.addContent(rule.getContent(), ResourceType.DRL);
        }
    }

    private void loadStaticRules() throws IOException {
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(DefaultRuleService.class.getClassLoader());

        Resource[] resources = resolver.getResources("/rules/*");
        for (Resource resource: resources) {
            String content = Files.asCharSource(resource.getFile(), Charsets.UTF_8).read();
            this.kieHelper.addContent(content, ResourceType.DRL);
        }
    }

    private void updateSession() {
        this.session = this.kieHelper.build(this.baseConfiguration).newKieSession();
//        this.session = this.kieContainer.newKieBase(this.baseConfiguration).newKieSession();
        this.insertServices();

        long ruleCount = this.session.getKieBase()
            .getKiePackages()
            .stream()
            .map(KiePackage::getRules)
            .map(Collection::size)
            .reduce(0, Integer::sum);
        LoggingService.info("Loaded " + ruleCount + " rules into Drools engine.");
    }

    private void insertServices() {
        this.session.setGlobal("logService", this.logService);
        this.session.setGlobal("alarmService", this.alarmService);
        this.session.setGlobal("ipAddressService", this.ipAddressService);
        this.session.setGlobal("platformService", this.platformService);
        this.session.setGlobal("softwareService", this.softwareService);
        this.session.setGlobal("userService", this.userService);
    }
}
