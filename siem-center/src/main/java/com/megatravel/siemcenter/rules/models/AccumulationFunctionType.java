package com.megatravel.siemcenter.rules.models;

public enum AccumulationFunctionType {
    SUM, AVERAGE, COUNT, MIN, MAX;
}
