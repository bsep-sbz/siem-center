package com.megatravel.siemcenter.rules.utils.interpreters;

import com.megatravel.siemcenter.common.util.FieldExpression;
import com.megatravel.siemcenter.common.util.FieldOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

@Component
public class FieldExpressionInterpreter implements Interpreter<FieldExpression> {

    private final References references;
    private final InterpretationErrors errors;

    @Autowired
    public FieldExpressionInterpreter(References references, InterpretationErrors errors) {
        this.references = references;
        this.errors = errors;
    }

    @Override
    public FieldExpression process(Object input) {
        if (!this.validate(input)) {
            return null;
        }

        Map obj = (Map) input;

        FieldExpression ex = new FieldExpression();

        ex.setFieldName((String) obj.get("field"));

        String fieldName = (String) obj.get("field");
        Optional<Field> field = Arrays.stream(Entities.activeEntity.getDeclaredFields())
            .filter(f -> f.getName().equals(fieldName))
            .findFirst();

        Optional<Method> method = Arrays.stream(Entities.activeEntity.getDeclaredMethods())
            .filter(m -> m.getName().equals(fieldName))
            .findFirst();

        if (!field.isPresent() && !method.isPresent()) {
            return null;
        } else if (field.isPresent()) {
            ex.setFieldType(field.get().getType());
        } else {
            ex.setFieldType(method.get().getReturnType());
        }

        String operation = (String) obj.get("operation");
        ex.setOperation(FieldOperation.valueOf(operation.toUpperCase()));

        ex.setValue(obj.get("value"));

        return ex;
    }

    public boolean validate(Object input) {
        Map obj = (Map) input;

        boolean valid = true;
        for (String key : Arrays.asList("field", "operation", "value")) {
            if (!obj.containsKey(key)) {
                errors.add("Field expression object missing field '" + key + "'.");
                valid = false;
            }
        }

        if (!(obj.get("field") instanceof String)) {
            errors.add("'field' must be a text value.");
            valid = false;
        } else {
            String fieldName = (String) obj.get("field");
            Optional<Field> field = Arrays.stream(Entities.activeEntity.getDeclaredFields())
                .filter(f -> f.getName().equals(fieldName))
                .findFirst();

            Optional<Method> method = Arrays.stream(Entities.activeEntity.getDeclaredMethods())
                .filter(m -> m.getName().equals(fieldName))
                .findFirst();

            if (!field.isPresent() && !method.isPresent()) {
                errors.add("'field' not declared for " + Entities.activeEntity.getName() + " entity.");
                valid = false;
            }
        }

        if (!(obj.get("operation") instanceof String)) {
            errors.add("'operation' must be a text value.");
            valid = false;
        } else {
            String operation = (String) obj.get("operation");
            try {
                FieldOperation.valueOf(operation.toUpperCase());
            } catch (IllegalArgumentException e) {
                errors.add("Operation '" + operation + "' is invalid. Available field operations are: 'neq', 'eq', " +
                    "'lt', 'le', 'ge', 'gt' and 'regex'.");
                valid = false;
            }
        }

        if (!(obj.get("value") instanceof String)) {
            errors.add("'value' must be a text value.");
            valid = false;
        } else {
            String value = (String) obj.get("value");
            if (value.startsWith("$")) {
                if (value.contains(" ")) {
                    errors.add("Reference  '" + value + "' should not contain white space characters.");
                    valid = false;
                } else {
                    if (!this.references.has(value)) {
                        errors.add("Reference '" + value + "' not recognized.");
                        valid = false;
                    }
                }
            }
        }

        return valid;
    }
}
