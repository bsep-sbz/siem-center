package com.megatravel.siemcenter.rules.utils.interpreters;

import com.megatravel.siemcenter.common.util.LogicalExpression;
import com.megatravel.siemcenter.common.util.LogicalOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class LogicalExpressionInterpreter implements Interpreter<LogicalExpression> {

    private final InterpretationErrors errors;
    private final ExpressionInterpreter expressionCompiler;

    @Autowired
    public LogicalExpressionInterpreter(InterpretationErrors errors, ExpressionInterpreter expressionCompiler) {
        this.errors = errors;
        this.expressionCompiler = expressionCompiler;
    }

    @Override
    public LogicalExpression process(Object input) {
        if (!this.validate(input)){
            return null;
        }

        Map obj = (Map) input;

        LogicalExpression ex = new LogicalExpression();

        String key = (String) obj.keySet().iterator().next();
        ex.setOperation(LogicalOperation.valueOf(key.toUpperCase()));


        for(Object expression: (List) obj.get(key)) {
            ex.getParams().add(expressionCompiler.process(expression));
        }

        return ex;
    }

    public boolean validate(Object input) {
        if (!(input instanceof Map)) {
            errors.add("Logical expression must be a JSON object.");
            return false;
        }

        Map obj = (Map) input;

        if (obj.keySet().size() == 0) {
            errors.add("Logical expression must not be an empty JSON object.");
            return false;
        }

        boolean valid = true;
        String key = (String) obj.keySet().iterator().next();
        try {
            LogicalOperation.valueOf(key.toUpperCase());
        } catch (IllegalArgumentException e) {
            errors.add("Logical operation '" + key + "' is invalid. Available logical operations are: 'and', 'or' " +
                "and 'not'.");
            valid = false;
        }

        if (!(obj.get(key) instanceof List)) {
            errors.add("Logical expression must be used on collection of JSON objects.");
            valid = false;
        }

        return valid;
    }
}
