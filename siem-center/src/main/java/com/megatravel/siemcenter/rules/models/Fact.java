package com.megatravel.siemcenter.rules.models;

import com.megatravel.siemcenter.common.util.Expression;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Fact {
    private FactType type;
    private Class  entity;
    private String ref;
    private Map<String, String> fieldRefs;
    private List<Expression> constraints;
    private List<Expression> fieldConstraints;
    private Frame frame;
    private AccumulationFunction function;

    public Fact() {
        this.fieldRefs = new HashMap<>();
        this.constraints = new LinkedList<>();
        this.fieldConstraints = new LinkedList<>();
    }

    public FactType getType() {
        return type;
    }

    public void setType(FactType type) {
        this.type = type;
    }

    public Class getEntity() {
        return entity;
    }

    public void setEntity(Class entity) {
        this.entity = entity;
    }

    public Map<String, String> getFieldRefs() {
        return fieldRefs;
    }

    public void setFieldRefs(Map<String, String> fieldRefs) {
        this.fieldRefs = fieldRefs;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public List<Expression> getConstraints() {
        return constraints;
    }

    public void setConstraints(List<Expression> constraints) {
        this.constraints = constraints;
    }

    public List<Expression> getFieldConstraints() {
        return fieldConstraints;
    }

    public void setFieldConstraints(List<Expression> fieldConstraints) {
        this.fieldConstraints = fieldConstraints;
    }

    public Frame getFrame() {
        return frame;
    }

    public void setFrame(Frame frame) {
        this.frame = frame;
    }

    public AccumulationFunction getFunction() {
        return function;
    }

    public void setFunction(AccumulationFunction function) {
        this.function = function;
    }
}
