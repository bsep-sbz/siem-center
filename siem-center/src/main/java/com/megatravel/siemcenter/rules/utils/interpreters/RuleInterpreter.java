package com.megatravel.siemcenter.rules.utils.interpreters;

import com.megatravel.siemcenter.rules.models.Rule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class RuleInterpreter implements Interpreter<Rule> {

    private final InterpretationErrors errors;
    private final FactInterpreter factCompiler;
    private final ActionInterpreter actionInterpreter;

    @Autowired
    public RuleInterpreter(
            InterpretationErrors errors, FactInterpreter factCompiler, ActionInterpreter actionInterpreter
    ) {
        this.errors = errors;
        this.factCompiler = factCompiler;
        this.actionInterpreter = actionInterpreter;
    }

    @Override
    public Rule process(Object input) {
        if (!this.validate(input)) {
            return null;
        }

        @SuppressWarnings("unchecked")
        Map obj = (Map<String, Object>) input;

        Rule rule = new Rule((String) obj.get("name"));

        List facts = (List) obj.get("facts");
        for (Object fact: facts) {
            rule.getFacts().add(this.factCompiler.process(fact));
        }

        List actions = (List) obj.get("actions");
        for (Object action: actions) {
            rule.getActions().add(this.actionInterpreter.process(action));
        }

        return rule;
    }

    public boolean validate(Object input) {
        if (!(input instanceof Map)) {
            errors.add("Template must be a JSON object.");
            return false;
        }

        @SuppressWarnings("unchecked")
        Map obj = (Map<String, Object>) input;

        boolean valid = true;
        for(String key: new String[] { "name", "facts", "actions" }) {
            if (!obj.containsKey(key)) {
                errors.add("Template object missing field '" + key + "'.");
                valid = false;
            }
        }

        if(!(obj.get("facts") instanceof List)) {
            errors.add("'facts' must be a list of JSON objects.");
            valid = false;
        }

        if(!(obj.get("actions") instanceof List)) {
            errors.add("'actions' must be a list of JSON objects.");
            valid = false;
        }

        return valid;
    }
}
