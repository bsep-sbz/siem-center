package com.megatravel.siemcenter.rules.utils.interpreters;

import com.megatravel.siemcenter.rules.models.AccumulationFunction;
import com.megatravel.siemcenter.rules.models.AccumulationFunctionType;
import com.megatravel.siemcenter.rules.models.Fact;
import com.megatravel.siemcenter.rules.models.FactType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class FactInterpreter implements Interpreter<Fact> {

    private final InterpretationErrors errors;
    private final References references;
    private final FrameInterpreter frameCompiler;
    private final ExpressionInterpreter expressionCompiler;

    @Autowired
    public FactInterpreter(InterpretationErrors errors, References references, FrameInterpreter frameCompiler, ExpressionInterpreter expressionCompiler) {
        this.errors = errors;
        this.references = references;
        this.frameCompiler = frameCompiler;
        this.expressionCompiler = expressionCompiler;
    }

    @Override
    public Fact process(Object input) {
        if (!this.validate(input)) {
            return null;
        }

        @SuppressWarnings("unchecked")
        Map obj = (Map<String, Object>) input;

        Fact fact = new Fact();

        String type = (String)obj.get("type");
        fact.setType(FactType.valueOf(type.toUpperCase()));

        String entity = (String) obj.get("entity");
        fact.setEntity(Entities.get(entity));
        Entities.activeEntity = fact.getEntity();

        if (obj.containsKey("ref")) {
            String ref = (String) obj.get("ref");
            references.add(ref, Entities.get(entity));
            fact.setRef(ref);
        }

        if (obj.containsKey("fieldRefs")) {
            Map fieldRefs = (Map) obj.get("fieldRefs");

            for(Object key: fieldRefs.keySet()) {
                Class c = null;
                try {
                    c = fact.getEntity().getDeclaredField((String)key).getClass();
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                }
                this.references.add((String) fieldRefs.get(key), c);
                fact.getFieldRefs().put((String) key, (String) fieldRefs.get(key));
            }
        }

        if (obj.containsKey("constraints")) {
            if (fact.getType() == FactType.COLLECTION) {
                Entities.activeEntity = List.class;
            } else if (fact.getType() == FactType.ACCUMULATION) {
                Entities.activeEntity = Number.class;
            }

            List constraints = (List) obj.get("constraints");
            for(Object constraint: constraints) {
                fact.getConstraints().add(expressionCompiler.process(constraint));
            }
        }

        if (obj.containsKey("fieldConstraints")) {
            Entities.activeEntity = fact.getEntity();
            List fieldConstraints = (List) obj.get("fieldConstraints");
            for(Object constraint: fieldConstraints) {
                fact.getFieldConstraints().add(expressionCompiler.process(constraint));
            }
        }

        if (obj.containsKey("frame")) {
            Object frame = obj.get("frame");
            fact.setFrame(this.frameCompiler.process(frame));
        }

        if (obj.containsKey("function")) {
            Map function = (Map) obj.get("function");

            String functionType = (String) function.get("type");
            String ref = (String) function.get("ref");

            AccumulationFunction func = new AccumulationFunction(
                AccumulationFunctionType.valueOf(functionType.toUpperCase()), ref
            );
            fact.setFunction(func);
        }

        return fact;
    }

    public boolean validate(Object input) {
        if (!(input instanceof Map)) {
            errors.add("Fact must be a JSON object.");
            return false;
        }

        @SuppressWarnings("unchecked")
        Map obj = (Map<String, Object>) input;

        boolean valid = true;
        for(String key: new String[] { "type", "entity"}) {
            if (!obj.containsKey(key)) {
                errors.add("Fact object is missing field '" + key + "'.");
                valid = false;
            }
        }

        if (!(obj.get("type") instanceof String)) {
            errors.add("'type' must be a text value.");
            valid = false;
        } else {
            String type = (String)obj.get("type");
            try {
                FactType.valueOf(type.toUpperCase());
            } catch (IllegalArgumentException e) {
                errors.add("Fact type '" + type + "' is invalid. Available fact types are: 'singular', 'collection' and 'accumulation'.");
                valid = false;
            }
        }

        if (!(obj.get("entity") instanceof String)) {
            errors.add("'entity' must be a text value.");
            valid = false;
        } else {
            String entity = (String) obj.get("entity");
            if (!Entities.has(entity)) {
                errors.add("Entity '" + entity + "' not recognized.");
                valid = false;
            }
        }

        if (!valid) {
            return false;
        }


        if (obj.containsKey("ref")) {
            if(!(obj.get("ref") instanceof String)) {
                errors.add("'ref' must be a text value.");
                valid = false;
            } else {
                String ref = (String) obj.get("ref");
                if (references.has(ref)) {
                    errors.add("Reference '" + ref + "' already used. Reference must be unique, please choose a different one.");
                    valid = false;
                }
            }
        }

        Class entity = Entities.get((String) obj.get("entity"));

        if (obj.containsKey("fieldRefs")) {
            if (!(obj.get("fieldRefs") instanceof Map)) {
                errors.add("'fieldRefs' field must be a JSON object.");
                valid = false;
            } else {
                Map fieldRefs = (Map) obj.get("fieldRefs");

                List<String> fieldNames = Arrays.stream(entity.getDeclaredFields())
                        .map(Field::getName)
                        .collect(Collectors.toList());

                for(Object key: fieldRefs.keySet()) {
                    if (!fieldNames.contains(key)) {
                        errors.add("'" + key + "' is not a valid '" + (String) obj.get("entity") + "' field.");
                        valid = false;
                    }
                }

                for(Object ref: fieldRefs.values()) {
                    if (references.has((String) ref)) {
                        errors.add("Reference '" + ref + "' already used. Reference must be unique, please choose a different one.");
                        valid = false;
                    }
                }
            }
        }

        if (obj.containsKey("constraints") && !(obj.get("constraints") instanceof List)) {
            errors.add("'constraints' field must be a list of JSON objects.");
            valid = false;
        }

        if (obj.containsKey("fieldConstraints") && !(obj.get("fieldConstraints") instanceof List)) {
            errors.add("'fieldConstraints' field must be a list of JSON objects.");
            valid = false;
        }

        if (obj.containsKey("frame") && !(obj.get("frame") instanceof Map)) {
            errors.add("'frame' field must be a JSON object.");
            valid = false;
        }

        if (obj.containsKey("function")) {
            String type = (String)obj.get("type");
            if (!type.equalsIgnoreCase(FactType.ACCUMULATION.name())) {
                errors.add("Invalid argument 'function' for type + '" + type + "'.");
                valid = false;
            }

            if (!(obj.get("function") instanceof Map)) {
                errors.add("'function' field must be a JSON object.");
                valid = false;
            } else {
                Map function = (Map) obj.get("function");
                for(String key: new String[] { "type", "entity"}) {
                    if (!obj.containsKey(key)) {
                        errors.add("Fact object is missing field '" + key + "'.");
                        valid = false;
                    }
                }

                if (function.containsKey("type")) {
                    String functionType = (String) function.get("type");

                    try {
                        AccumulationFunctionType.valueOf(functionType.toUpperCase());
                    } catch (IllegalArgumentException e) {
                        errors.add("Accumulation function type '" + functionType + "' is invalid. Available function types are: 'min', 'max', 'average', 'sum' and 'count'.");
                        valid = false;
                    }
                }

                if (function.containsKey("ref")) {
                    if(!(obj.get("ref") instanceof String)) {
                        errors.add("'ref' must be a text value.");
                        valid = false;
                    } else {
                        String ref = (String) function.get("ref");

                        if (ref.contains(" ")) {
                            errors.add("Reference  '" + ref + "' should not contain white space characters.");
                            valid = false;
                        } else {
//                            if (!this.references.has(ref)) {
//                                errors.add("Reference '" + ref + "' not recognized.");
//                                valid = false;
//                            }
                        }
                    }
                }
            }


        }

        return valid;
    }
}
