package com.megatravel.siemcenter.rules.utils.generators;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.megatravel.siemcenter.common.util.Expression;
import com.megatravel.siemcenter.rules.models.AccumulationFunction;
import com.megatravel.siemcenter.rules.models.Fact;
import com.megatravel.siemcenter.rules.models.Frame;
import com.megatravel.siemcenter.rules.models.FrameType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FactGenerator extends GenericGenerator<Fact> {

    private StringBuilder builder;
    private final ConstraintGenerator constraintGenerator;

    @Autowired
    public FactGenerator(Imports imports, ConstraintGenerator constraintGenerator) {
        super(imports);
        this.builder = new StringBuilder();
        this.constraintGenerator = constraintGenerator;
    }

    public String generate(Fact fact) {
        builder = new StringBuilder();

        if (fact.getRef() != null) {
            builder.append('$');
            builder.append(fact.getRef());
            builder.append(": ");
        }

        switch (fact.getType()) {
            case SINGULAR:
                this.generateSingular(fact);
                break;
            case COLLECTION:
                this.generateCollection(fact);
                break;
            case ACCUMULATION:
                this.generateAccumulation(fact);
        }

        return builder.toString();
    }

    private void generateSingular(Fact fact) {
        String name = fact.getEntity().getName();

        this.imports.add(name);
        builder.append(name.substring(name.lastIndexOf('.') + 1));
        builder.append('(');

        List<String> parts = new LinkedList<>();
        if (fact.getFieldRefs().size() != 0) {
            Map<String, String> refs = fact.getFieldRefs();
            for(Map.Entry<String, String> entry: refs.entrySet()) {
                parts.add( "$" + entry.getValue()+ " : " + entry.getKey());
            }
        }

        if (fact.getFieldConstraints().size() != 0) {
            for (Expression constraint: fact.getFieldConstraints()) {
                parts.add(constraintGenerator.generate(constraint));
            }
        }

        if (parts.size() == 1) {
            builder.append(parts.get(0));
        } else {
            String indent = this.getIndentation(this.indentLevel + 1);
            builder.append('\n');
            builder.append(
                parts.stream()
                    .map(part -> indent + part)
                    .collect(Collectors.joining(",\n"))
            );
            builder.append('\n');
            builder.append(this.getIndentation(this.indentLevel));
        }

        builder.append(')');

        if (fact.getFrame() != null) {
            this.generateFrame(fact.getFrame());
        }
    }

    private void generateCollection(Fact fact) {
        builder.append("List(");

        if (fact.getConstraints().size() != 0) {
            List<String> parts = new LinkedList<>();
            for (Expression constraint: fact.getConstraints()) {
                parts.add(constraintGenerator.generate(constraint));
            }

            if (parts.size() == 1) {
                builder.append(parts.get(0));
            } else {
                String indent = this.getIndentation(this.indentLevel + 1);
                builder.append('\n');
                builder.append(
                        parts.stream()
                                .map(part -> indent + part)
                                .collect(Collectors.joining(",\n"))
                );
                builder.append('\n');
                builder.append(this.getIndentation(this.indentLevel));
            }
        }

        builder.append(") from collect(");
        builder.append('\n');

        this.indentLevel += 1;
        builder.append(this.getIndentation(this.indentLevel));
        this.generateSingular(fact);
        this.indentLevel -= 1;

        builder.append('\n');
        builder.append(this.getIndentation(this.indentLevel));
        builder.append(')');
    }

    private void generateAccumulation(Fact fact) {
        builder.append("Number(");

        if (fact.getConstraints().size() != 0) {
            List<String> parts = new LinkedList<>();
            for (Expression constraint: fact.getConstraints()) {
                parts.add(constraintGenerator.generate(constraint));
            }

            if (parts.size() == 1) {
                builder.append(parts.get(0));
            } else {
                String indent = this.getIndentation(this.indentLevel + 1);
                builder.append('\n');
                builder.append(
                        parts.stream()
                                .map(part -> indent + part)
                                .collect(Collectors.joining(",\n"))
                );
                builder.append('\n');
                builder.append(this.getIndentation(this.indentLevel));
            }
        }

        builder.append(") from accumulate(");
        builder.append('\n');

        this.indentLevel += 1;

        builder.append(this.getIndentation(this.indentLevel));
        this.generateSingular(fact);

        builder.append(",\n");
        builder.append(this.getIndentation(this.indentLevel));

        AccumulationFunction function = fact.getFunction();
        builder.append(function.getType().name().toLowerCase());
        builder.append('(');
        builder.append(function.getRef());
        builder.append(')');

        this.indentLevel -= 1;

        builder.append('\n');
        builder.append(this.getIndentation(this.indentLevel));
        builder.append(')');
    }

    private void generateFrame(Frame frame) {
        builder.append(" over window:");
        if (frame.getType() == FrameType.LENGTH) {
            builder.append("length");
        } else {
            builder.append("time");
        }
        builder.append('(');
        builder.append(frame.getValue());
        builder.append(')');
    }
}
