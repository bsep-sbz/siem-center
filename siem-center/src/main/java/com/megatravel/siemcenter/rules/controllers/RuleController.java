package com.megatravel.siemcenter.rules.controllers;

import java.io.IOException;
import java.util.Map;

import com.megatravel.siemcenter.common.http.exceptions.BadRequest;
import com.megatravel.siemcenter.rules.exceptions.RuleInterpretationException;
import com.megatravel.siemcenter.rules.services.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/config/templates")
@CrossOrigin(origins = "http://localhost:8080")
public class RuleController {

    private final RuleService service;

    @Autowired
    public RuleController(RuleService service) {
        this.service = service;
    }

    @PostMapping
    public void create(@RequestBody Map<String, Object> data) throws BadRequest {
        try {
            this.service.createRule(data);
        } catch (RuleInterpretationException | IOException e) {
            throw new BadRequest(e.getMessage());
        }
    }
}
