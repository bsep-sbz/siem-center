package com.megatravel.siemcenter.rules.models;

import java.util.HashMap;
import java.util.Map;

public class Action {
    private ActionType type;
    private Map<String, Object> args;

    public Action() {
        this.args = new HashMap<>();
    }

    public Action(ActionType type) {
        this();
        this.type = type;
    }

    public ActionType getType() {
        return type;
    }

    public void setType(ActionType type) {
        this.type = type;
    }

    public Map<String, Object> getArgs() {
        return args;
    }

    public void setArgs(Map<String, Object> args) {
        this.args = args;
    }
}
