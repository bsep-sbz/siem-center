package com.megatravel.siemcenter.rules.models;

public enum FrameType {
    TIME, LENGTH;
}
