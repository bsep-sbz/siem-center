package com.megatravel.siemcenter.rules.utils.interpreters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class InsertDeleteActionInterpreter implements Interpreter<Map<String, Object>> {

    private final InterpretationErrors errors;
    private final References references;

    @Autowired
    public InsertDeleteActionInterpreter(InterpretationErrors errors, References references) {
        this.errors = errors;
        this.references = references;
    }

    @Override
    public Map<String, Object> process(Object input) {
        if(!this.validate(input)) {
            return null;
        }

        @SuppressWarnings("unchecked")
        Map<String, Object> obj = (Map<String, Object>) input;
        return obj;
    }

    public boolean validate(Object input) {
        if(!(input instanceof Map)) {
            errors.add("Delete action args must be a JSON object.");
            return false;
        }

        Map obj = (Map) input;

        if(!obj.containsKey("ref")) {
            return false;
        }

        boolean valid = true;
        if(!(obj.get("ref") instanceof String)) {
            errors.add("'ref' must be a text value.");
            valid = false;
        } else {
            String ref = (String) obj.get("ref");
            if (!this.references.has(ref)) {
                errors.add("Reference '" + ref + "' not recognized.");
                valid = false;
            }
        }

        return valid;
    }
}
