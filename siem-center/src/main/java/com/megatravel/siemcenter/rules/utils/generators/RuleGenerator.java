package com.megatravel.siemcenter.rules.utils.generators;

import com.megatravel.siemcenter.rules.models.Rule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


@Component
public class RuleGenerator {

    private final FactGenerator factGenerator;
    private final ActionGenerator actionGenerator;

    @Autowired
    public RuleGenerator(FactGenerator factGenerator, ActionGenerator actionGenerator) {
        this.factGenerator = factGenerator;
        this.factGenerator.indentLevel = 1;
        this.actionGenerator = actionGenerator;
        this.actionGenerator.indentLevel = 1;
    }

    public String[] generate(Rule rule) {
        StringBuilder builder = new StringBuilder();

        List<String> facts = rule.getFacts()
            .stream()
            .map(this.factGenerator::generate)
            .collect(Collectors.toList());

        List<String> actions = rule.getActions()
            .stream()
            .map(this.actionGenerator::generate)
            .collect(Collectors.toList());


        StringBuilder factsBuilder = new StringBuilder();
        for(String fact: facts) {
            factsBuilder.append('\t');
            factsBuilder.append(fact);
            factsBuilder.append('\n');
        }

        StringBuilder actionsBuilder = new StringBuilder();
        for(String action: actions) {
            actionsBuilder.append('\t');
            actionsBuilder.append(action);
            actionsBuilder.append('\n');
        }

        return new String[] {
            rule.getName(),
            "1",
            factsBuilder.toString(),
            actionsBuilder.toString()
        };
    }
}
