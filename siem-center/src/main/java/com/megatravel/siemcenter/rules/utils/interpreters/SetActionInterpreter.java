package com.megatravel.siemcenter.rules.utils.interpreters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Map;

@Component
public class SetActionInterpreter implements Interpreter<Map<String, Object>> {

    private final InterpretationErrors errors;
    private final References references;


    @Autowired
    public SetActionInterpreter(InterpretationErrors errors, References refTable) {
        this.errors = errors;
        this.references = refTable;
    }

    @Override
    public Map<String, Object> process(Object input) {
        if(!this.validate(input)) {
            return null;
        }

        @SuppressWarnings("unchecked")
        Map<String, Object> obj = (Map<String, Object>) input;
        return obj;
    }

    public boolean validate(Object input) {
        if(!(input instanceof Map)) {
            errors.add("Set action args must be a JSON object.");
            return false;
        }

        Map obj = (Map) input;

        boolean valid = true;
        for (String key: Arrays.asList("ref", "field", "value")) {
            if (!obj.containsKey(key)) {
                errors.add("Set action args object is missing field '" + key + "'.");
                valid = false;
            }
        }

        if(!valid) {
            return false;
        }

        if(!(obj.get("ref") instanceof String)) {
            errors.add("'ref' must be a text value.");
            valid = false;
        } else {
            String ref = (String) obj.get("ref");
            if (!this.references.has(ref)) {
                errors.add("Reference '" + ref + "' not recognized.");
                valid = false;
            }
        }

        // TODO: check the type of field

        if(!(obj.get("field") instanceof String)) {
            errors.add("'field' must be a text value.");
            valid = false;
        }

//        if(!(obj.get("value") instanceof String)) {
//            throw new InvalidDocumentException();
//        }

        if (!(obj.get("value") instanceof String)) {
            errors.add("'value' must be a text value.");
            valid = false;
        } else {
            String value = (String) obj.get("value");
            if (value.startsWith("$")) {
                if (value.contains(" ")) {
                    errors.add("Reference  '" + value + "' should not contain white space characters.");
                    valid = false;
                } else {
                    if (!this.references.has(value)) {
                        errors.add("Reference '" + value + "' not recognized.");
                        valid = false;
                    }
                }
            }
        }

        return valid;
    }
}
