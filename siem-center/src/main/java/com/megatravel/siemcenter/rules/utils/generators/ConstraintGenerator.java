package com.megatravel.siemcenter.rules.utils.generators;

import com.megatravel.siemcenter.common.util.Expression;
import com.megatravel.siemcenter.common.util.FieldExpression;
import com.megatravel.siemcenter.common.util.LogicalExpression;
import com.megatravel.siemcenter.common.util.LogicalOperation;
import org.apache.commons.lang3.ClassUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConstraintGenerator extends GenericGenerator<Expression> {

    @Autowired
    public ConstraintGenerator(Imports imports) {
        super(imports);
    }

    @Override
    public String generate(Expression data) {
        if (data instanceof LogicalExpression) {
            return this.generateLogicalExpression((LogicalExpression) data);
        } else {
            return this.generateFieldExpression((FieldExpression) data);
        }
    }

    private String generateLogicalExpression(LogicalExpression data) {
        StringBuilder builder = new StringBuilder();

        if (data.getOperation() == LogicalOperation.NOT) {
            builder.append("!(");
            builder.append(data.getParams().get(0));
            builder.append(')');
            return builder.toString();
        }

        int n = data.getParams().size();
        for (int i = 0; i < n - 1; i++) {
            builder.append('(');
            builder.append(this.generate(data.getParams().get(i)));
            builder.append(')');

            switch (data.getOperation()) {
                case AND:
                    builder.append(" && ");
                    break;
                case OR:
                    builder.append(" || ");
                    break;
                case NOT:
                    builder.append("!");
            }
        }
        builder.append('(');
        builder.append(this.generate(data.getParams().get(n - 1)));
        builder.append(')');
        return builder.toString();
    }

    private String generateFieldExpression(FieldExpression expression) {
        if (ClassUtils.isPrimitiveOrWrapper(expression.getFieldType()) ||
            expression.getFieldType() == String.class
        ) {
            return generatePrimitiveFieldExpression(expression);
        } else {
            return generateComparableFieldExpression(expression);
        }
    }

    private String generatePrimitiveFieldExpression(FieldExpression expression) {
        StringBuilder builder = new StringBuilder();
        builder.append(expression.getFieldName());

        builder.append(' ');

        switch (expression.getOperation()) {
            case EQ:
                builder.append("==");
                break;
            case GE:
                builder.append(">=");
                break;
            case GT:
                builder.append(">");
                break;
            case LE:
                builder.append("<=");
                break;
            case LT:
                builder.append("<");
                break;
            case NEQ:
                builder.append("!=");
                break;
            case REGEX:
                builder.append("matches");
                break;
        }

        builder.append(' ');

        String value = (String) expression.getValue();
        if (value.startsWith("$")) {
            builder.append(value);
        } else if (
            Number.class.isAssignableFrom(expression.getFieldType()) ||
                expression.getFieldType() == int.class ||
                expression.getFieldType() == float.class ||
                expression.getFieldType() == double.class ||
                expression.getFieldType() == long.class ||
                expression.getFieldType() == byte.class
        ) {
            builder.append(value);
        } else {
            builder.append('"');
            builder.append(value);
            builder.append('"');
        }

        return builder.toString();
    }

    private String generateComparableFieldExpression(FieldExpression expression) {
        StringBuilder builder = new StringBuilder();
        builder.append(expression.getFieldName());

        builder.append(".compareTo(");

        Object value = expression.getValue();
        if (value instanceof String && !((String) value).startsWith("$")) {
            builder.append('"');
            builder.append(value);
            builder.append('"');
        } else {
            builder.append(value);
        }

        builder.append(") ");

        switch (expression.getOperation()) {
            case EQ:
                builder.append("==");
                break;
            case GE:
                builder.append(">=");
                break;
            case GT:
                builder.append(">");
                break;
            case LE:
                builder.append("<=");
                break;
            case LT:
                builder.append("<");
                break;
            case NEQ:
                builder.append("!=");
                break;
        }

        builder.append(" 0");

        return builder.toString();
    }
}
