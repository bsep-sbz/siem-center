package com.megatravel.siemcenter.rules.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Rule {
    @Id
    private String id;
    private String name;
    private String content;

    @Transient
    private List<Fact> facts;
    @Transient
    private List<Action> actions;

    public Rule() {
        facts = new ArrayList<>();
        actions = new ArrayList<>();
    }

    public Rule(String name) {
        this();
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Fact> getFacts() {
        return facts;
    }

    public void setFacts(List<Fact> facts) {
        this.facts = facts;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

}
