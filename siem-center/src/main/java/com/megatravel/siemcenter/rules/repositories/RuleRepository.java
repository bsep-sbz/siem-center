package com.megatravel.siemcenter.rules.repositories;

import com.megatravel.siemcenter.rules.models.Rule;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RuleRepository extends MongoRepository<Rule, String> {
}
