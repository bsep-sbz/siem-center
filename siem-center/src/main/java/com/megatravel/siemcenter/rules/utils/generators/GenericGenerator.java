package com.megatravel.siemcenter.rules.utils.generators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GenericGenerator<T> implements Generator<T> {

    protected final Imports imports;
    protected int indentLevel;

    @Autowired
    public GenericGenerator(Imports imports) {
        this.imports = imports;
        this.indentLevel = 0;
    }

    @Override
    public String generate(T data) {
        return data.toString();
    }

    protected String getIndentation(int level) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < level; i++) {
            sb.append('\t');
        }
        return sb.toString();
    }
}
