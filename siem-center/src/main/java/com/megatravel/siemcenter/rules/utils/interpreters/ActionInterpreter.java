package com.megatravel.siemcenter.rules.utils.interpreters;

import com.megatravel.siemcenter.rules.models.Action;
import com.megatravel.siemcenter.rules.models.ActionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Map;


@Component
public class ActionInterpreter implements Interpreter<Action> {

    private final InterpretationErrors errors;

    private final InsertDeleteActionInterpreter insertDeleteActionCompiler;
    private final NewActionInterpreter newActionCompiler;
    private final SetActionInterpreter setActionCompiler;
    private final GetActionInterpreter getActionCompiler;

    @Autowired
    public ActionInterpreter(InterpretationErrors errors, InsertDeleteActionInterpreter insertDeleteActionCompiler, NewActionInterpreter newActionCompiler, SetActionInterpreter setActionCompiler, GetActionInterpreter getActionCompiler) {
        this.errors = errors;
        this.insertDeleteActionCompiler = insertDeleteActionCompiler;
        this.newActionCompiler = newActionCompiler;
        this.setActionCompiler = setActionCompiler;
        this.getActionCompiler = getActionCompiler;
    }

    @Override
    public Action process(Object input) {
        if (!this.validate(input)) {
            return null;
        }

        Map obj = (Map) input;
        Action action = new Action();

        String type = (String) obj.get("type");
        action.setType(ActionType.valueOf(type.toUpperCase()));

        @SuppressWarnings("unchecked")
        Map<String, Object> args;

        switch (action.getType()) {
            case NEW:
                args = this.newActionCompiler.process(obj.get("args"));
                break;
            case GET:
                args = this.getActionCompiler.process(obj.get("args"));
                break;
            case SET:
                args = this.setActionCompiler.process(obj.get("args"));
                break;
            case INSERT:
            case DELETE:
                args = this.insertDeleteActionCompiler.process(obj.get("args"));
                break;
            default:
                args = null;
        }

        action.setArgs(args);

        return action;
    }

    public boolean validate(Object input) {
        if (!(input instanceof Map)) {
            errors.add("Action must be a JSON object.");
            return false;
        }

        Map obj = (Map) input;

        boolean valid = true;
        for(String key: Arrays.asList("type", "args")) {
            if (!obj.containsKey(key)) {
                errors.add("Action object is missing field '" + key + "'.");
                valid = false;
            }
        }

        if (!(obj.get("type") instanceof String)) {
            errors.add("'type' must be a text value.");
            valid = false;
        }

        String type = (String) obj.get("type");
        try {
            ActionType.valueOf(type.toUpperCase());
        } catch (IllegalArgumentException e) {
            errors.add("Action type '" + type + "' is invalid. Available action types are: 'insert', 'modify', 'delete', 'new', 'get' and 'set'.");
            valid = false;
        }

        if (!(obj.get("args") instanceof Map)) {
            errors.add("'args' must be a JSON object.");
            valid = false;
        }

        return valid;
    }
}
