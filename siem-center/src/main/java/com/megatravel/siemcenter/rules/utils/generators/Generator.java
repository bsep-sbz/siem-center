package com.megatravel.siemcenter.rules.utils.generators;

public interface Generator<T> {

    String generate(T data);
}
