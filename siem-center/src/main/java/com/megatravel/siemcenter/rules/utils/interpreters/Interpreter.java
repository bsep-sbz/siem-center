package com.megatravel.siemcenter.rules.utils.interpreters;

public interface Interpreter<T> {
    T process(Object input);
    boolean validate(Object input);
}
