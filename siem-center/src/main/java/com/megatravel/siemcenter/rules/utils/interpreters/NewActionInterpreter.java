package com.megatravel.siemcenter.rules.utils.interpreters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Map;

@Component
public class NewActionInterpreter implements Interpreter<Map<String, Object>> {

    private final InterpretationErrors errors;
    private final References references;

    @Autowired
    public NewActionInterpreter(InterpretationErrors errors, References references) {
        this.errors = errors;
        this.references = references;
    }

    @Override
    public Map<String, Object> process(Object input) {
        if(!this.validate(input)) {
            return null;
        }

        @SuppressWarnings("unchecked")
        Map<String, Object> obj = (Map<String, Object>) input;

        String entity = (String) obj.get("entity");
        String ref = (String) obj.get("ref");
        this.references.add(ref, Entities.get(entity));

        return obj;
    }

    public boolean validate(Object input) {
        if(!(input instanceof Map)) {
            errors.add("Get action args must be a JSON object.");
            return false;
        }

        Map obj = (Map) input;

        boolean valid = true;
        for (String key: Arrays.asList("entity", "ref")) {
            if (!obj.containsKey(key)) {
                errors.add("Action args object is missing field '" + key + "'.");
                valid = false;
            }
        }

        if(!valid) {
            return false;
        }

        if(!(obj.get("entity") instanceof String)) {
            errors.add("'entity' must be a text value.");
            valid = false;
        } else {
            String entity = (String) obj.get("entity");
            if (!Entities.has(entity)) {
                errors.add("Entity '" + entity + "' not recognized.");
                valid = false;
            }
        }

        if(!(obj.get("ref") instanceof String)) {
            errors.add("'ref' must be a text value.");
            valid = false;
        } else {
            String ref = (String) obj.get("ref");
            if (this.references.has(ref)) {
                errors.add("Reference '" + ref + "' already used. Reference must be unique, please choose a different one.");
                valid = false;
            }
        }

        return valid;
    }
}
