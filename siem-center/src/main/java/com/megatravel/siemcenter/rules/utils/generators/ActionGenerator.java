package com.megatravel.siemcenter.rules.utils.generators;

import com.megatravel.siemcenter.alarms.models.Alarm;
import com.megatravel.siemcenter.logs.models.Log;
import com.megatravel.siemcenter.rules.models.Action;
import com.megatravel.siemcenter.rules.utils.interpreters.References;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ActionGenerator extends GenericGenerator<Action> {

    private StringBuilder builder;
    private final References references;

    @Autowired
    public ActionGenerator(Imports imports, References references) {
        super(imports);
        this.builder = new StringBuilder();
        this.references = references;
    }

    @Override
    public String generate(Action data) {
        this.builder = new StringBuilder();

        switch (data.getType()) {
            case NEW:
                this.generateNew(data);
                break;
            case GET:
                this.generateGet(data);
                break;
            case SET:
                this.generateSet(data);
                break;
            case DELETE:
                this.generateDelete(data);
                break;
            case INSERT:
                this.generateInsert(data);
                break;
            case MODIFY:
                this.generateModify(data);
        }
        return builder.toString();
    }

    private void generateNew(Action action) {
        String entity = (String) action.getArgs().get("entity");
        String ref = (String) action.getArgs().get("ref");

        builder.append(entity);
        builder.append(" $");
        builder.append(ref);
        builder.append(" = new ");
        builder.append(entity);
        builder.append("();");
    }

    private void generateSet(Action action) {
        String ref = (String) action.getArgs().get("ref");
        String field = (String) action.getArgs().get("field");
        String value = (String) action.getArgs().get("value");

        builder.append(ref);
        builder.append(".set");
        builder.append(Character.toUpperCase(field.charAt(0)));
        builder.append(field.substring(1));

        builder.append("(");
        if (value.startsWith("$")) {
            builder.append(value);
        } else {
            builder.append('"');
            builder.append(value);
            builder.append('"');
        }
        builder.append(");");
    }

    private void generateGet(Action action) {
        String ref = (String) action.getArgs().get("ref");
        String field = (String) action.getArgs().get("field");

        builder.append(ref);
        builder.append(".get");
        builder.append(Character.toUpperCase(field.charAt(0)));
        builder.append(field.substring(1));
        builder.append('(');
        builder.append(");");
    }

    private void generateDelete(Action action) {
        String ref = (String) action.getArgs().get("ref");
        builder.append("delete(");
        builder.append(ref);
        builder.append(");");

        this.saveRef(ref);
    }

    private void generateInsert(Action action) {
        String ref = (String) action.getArgs().get("ref");

        builder.append("insert(");
        builder.append(ref);
        builder.append(");");

        this.saveRef(ref);
    }

    private void generateModify(Action action) {

    }

    private void saveRef(String ref) {
        Class entity = this.references.get(ref);

        builder.append('\n');
        builder.append('\t');
        if (entity == Alarm.class) {
            builder.append("alarmService.save(");
        } else if (entity == Log.class){
            builder.append("logService.save(");
        }
        builder.append(ref);
        builder.append(");");
    }
}
