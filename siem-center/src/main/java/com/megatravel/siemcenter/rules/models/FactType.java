package com.megatravel.siemcenter.rules.models;

public enum  FactType {
    SINGULAR, COLLECTION, ACCUMULATION;
}
