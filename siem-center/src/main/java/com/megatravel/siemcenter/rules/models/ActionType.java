package com.megatravel.siemcenter.rules.models;

public enum ActionType {
    INSERT, MODIFY, DELETE, NEW, SET, GET;
}
