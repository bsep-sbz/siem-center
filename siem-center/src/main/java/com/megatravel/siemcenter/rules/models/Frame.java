package com.megatravel.siemcenter.rules.models;

public class Frame {
    private FrameType type;
    private String value;

    public FrameType getType() {
        return type;
    }

    public void setType(FrameType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
