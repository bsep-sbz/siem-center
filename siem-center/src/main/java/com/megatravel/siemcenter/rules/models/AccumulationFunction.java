package com.megatravel.siemcenter.rules.models;

public class AccumulationFunction {
    private AccumulationFunctionType type;
    private String ref;

    public AccumulationFunction() {

    }

    public AccumulationFunction(AccumulationFunctionType type, String ref) {
        this.type = type;
        this.ref = ref;
    }

    public AccumulationFunctionType getType() {
        return type;
    }

    public void setType(AccumulationFunctionType type) {
        this.type = type;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }
}
