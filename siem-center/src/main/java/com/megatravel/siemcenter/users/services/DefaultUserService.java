package com.megatravel.siemcenter.users.services;

import com.megatravel.siemcenter.common.models.RiskFactor;
import com.megatravel.siemcenter.users.models.User;
import com.megatravel.siemcenter.users.repositories.UserRepository;
import com.megatravel.siemcenter.users.requests.SaveUserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DefaultUserService implements UserService {

    private final UserRepository repository;

    @Autowired
    public DefaultUserService(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public User process(SaveUserRequest data) {
        return this.save(new User(data.username));
    }

    @Override
    public User save(User user) {
        return this.repository.save(user);
    }

    @Override
    public User save(String username) {
        return this.repository.findOneByUsername(username).orElseGet(
            () -> this.repository.save(new User(username))
        );
    }

    @Override
    public User update(String id, RiskFactor riskFactor) throws Exception {
        User user = this.findOne(id).orElseThrow(() -> new Exception("User not found"));
        user.setRiskFactor(riskFactor);
        return this.save(user);
    }

    @Override
    public Optional<User> findOne(String id) {
        return this.repository.findById(id);
    }

    @Override
    public Optional<User> findOneByUsername(String username) {
        return this.repository.findOneByUsername(username);
    }

    @Override
    public List<User> findAll() {
        return this.repository.findAll();
    }

    @Override
    public List<User> findAllAdmins() {
        return this.repository.findAllByAdminTrue();
    }

    @Override
    public void remove(String id) {
        this.repository.deleteById(id);
    }
}
