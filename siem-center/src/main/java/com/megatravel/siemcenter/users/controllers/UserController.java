package com.megatravel.siemcenter.users.controllers;

import com.megatravel.siemcenter.common.http.exceptions.NotFound;
import com.megatravel.siemcenter.common.models.RiskFactor;
import com.megatravel.siemcenter.users.models.User;
import com.megatravel.siemcenter.users.requests.SaveUserRequest;
import com.megatravel.siemcenter.users.requests.UpdateUserRequest;
import com.megatravel.siemcenter.users.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public void post(@Valid @RequestBody SaveUserRequest data) {
        this.userService.process(data);
    }

    @GetMapping
    public List<User> getAll() {
        return this.userService.findAll();
    }

    @PutMapping("/{id}")
    public User update(@PathVariable String id, @Valid @RequestBody UpdateUserRequest request) throws NotFound {
        try {
            return this.userService.update(id, request.riskFactor);
        } catch (Exception e) {
            throw new NotFound(e.getMessage());
        }
    }
}