package com.megatravel.siemcenter.users.services;

import com.megatravel.siemcenter.common.http.exceptions.NotFound;
import com.megatravel.siemcenter.common.models.RiskFactor;
import com.megatravel.siemcenter.users.models.User;
import com.megatravel.siemcenter.users.requests.SaveUserRequest;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User process(SaveUserRequest data);
    User save(User user);
    User save(String username);
    User update(String id, RiskFactor riskFactor) throws NotFound, Exception;
    Optional<User> findOne(String id);
    Optional<User> findOneByUsername(String username);
    List<User> findAll();
    List<User> findAllAdmins();
    void remove(String id);

}
