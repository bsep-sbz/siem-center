package com.megatravel.siemcenter.users.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.megatravel.siemcenter.common.models.RiskFactor;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.Objects;

public class User {
    @Id
    private String id;
    @Indexed(unique = true)
    private String username;
    private LocalDateTime lastActive;
    private RiskFactor riskFactor;

    @JsonIgnore
    private boolean admin;

    public User() {
        this.username = "unknown";
        this.riskFactor = RiskFactor.LOW;
        this.admin = false;
    }

    public User(String username) {
        this.username = username;
        this.riskFactor = RiskFactor.LOW;
        this.lastActive = LocalDateTime.now();
    }

    public User(String username, LocalDateTime lastActive, RiskFactor riskFactor) {
        this.username = username;
        this.lastActive = lastActive;
        this.riskFactor = riskFactor;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getLastActive() {
        return lastActive;
    }

    public void setLastActive(LocalDateTime lastActive) {
        this.lastActive = lastActive;
    }

    public RiskFactor getRiskFactor() {
        return riskFactor;
    }

    public void setRiskFactor(RiskFactor riskFactor) {
        this.riskFactor = riskFactor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(username, user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }
}
