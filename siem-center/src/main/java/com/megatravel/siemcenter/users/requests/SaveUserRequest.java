package com.megatravel.siemcenter.users.requests;

import javax.validation.constraints.NotBlank;

public class SaveUserRequest {
    @NotBlank
    public String username;
}
