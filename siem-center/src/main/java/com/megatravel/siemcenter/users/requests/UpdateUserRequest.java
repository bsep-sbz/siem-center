package com.megatravel.siemcenter.users.requests;

import com.megatravel.siemcenter.common.models.RiskFactor;

import javax.validation.constraints.NotNull;

public class UpdateUserRequest {
    @NotNull
    public RiskFactor riskFactor;
}
