package com.megatravel.siemcenter.users.repositories;

import com.megatravel.siemcenter.users.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
    Optional<User> findOneByUsername(String username);
    List<User> findAllByAdminTrue();
}
