package com.megatravel.siemcenter.reports.models;

import com.fasterxml.jackson.annotation.JsonValue;
import com.megatravel.siemcenter.common.models.IPAddress;

public class MachineReportItem {
    private String ip;
    private int count;

    public MachineReportItem(String ip, int count) {
        this.ip = ip;
        this.count = count;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
