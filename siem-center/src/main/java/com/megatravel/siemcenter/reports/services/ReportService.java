package com.megatravel.siemcenter.reports.services;

import com.megatravel.siemcenter.alarms.models.Alarm;
import com.megatravel.siemcenter.alarms.services.AlarmService;
import com.megatravel.siemcenter.common.models.IPAddress;
import com.megatravel.siemcenter.common.models.Software;
import com.megatravel.siemcenter.common.services.IPAddressService;
import com.megatravel.siemcenter.common.services.SoftwareService;
import com.megatravel.siemcenter.logs.models.Log;
import com.megatravel.siemcenter.logs.services.LogService;
import com.megatravel.siemcenter.reports.models.MachineReportItem;
import com.megatravel.siemcenter.reports.models.SystemReportItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@Service
public class ReportService {

    private final AlarmService alarmService;
    private final LogService logService;
    private final IPAddressService ipAddressService;
    private final SoftwareService softwareService;

    @Autowired
    public ReportService(AlarmService alarmService, LogService logService, IPAddressService ipAddressService, SoftwareService softwareService) {
        this.alarmService = alarmService;
        this.logService = logService;
        this.ipAddressService = ipAddressService;
        this.softwareService = softwareService;
    }

    public List<MachineReportItem> logsPerMachine(LocalDateTime start_time, LocalDateTime end_time) {
        List<MachineReportItem> report = new LinkedList<>();
        for(IPAddress ip: this.ipAddressService.findAll()) {
            report.add(new MachineReportItem(
                ip.getValue(), this.logService.numberOfLogsPerIp(ip, start_time, end_time)
            ));
        }
        return report;
    }


    public List<SystemReportItem> logsPerSystem(LocalDateTime start_time, LocalDateTime end_time) {
        List<SystemReportItem> report = new LinkedList<>();
        for(Software software: this.softwareService.findAll()) {
            report.add(new SystemReportItem(
                software.getValue(), this.logService.numberOfLogsPerSoftware(software, start_time, end_time)
            ));
        }
        return report;
    }

    public List<MachineReportItem> alarmsPerMachine(LocalDateTime start_time, LocalDateTime end_time) {
        List<MachineReportItem> report = new LinkedList<>();

        Map<IPAddress, Integer> counter = new HashMap<>();
        for (Alarm alarm: this.alarmService.findAll()) {
            for (Log log: alarm.getAssociatedLogs()) {
                if (log.getTimestamp().getValue().isAfter(start_time) && log.getTimestamp().getValue().isBefore(end_time)) {
                    counter.put(log.getIp(), counter.getOrDefault(log.getIp(), 0) + 1);
                }
            }
        }

        for (Map.Entry<IPAddress, Integer> entry: counter.entrySet()) {
            report.add(new MachineReportItem(entry.getKey().getValue(), entry.getValue()));
        }

        return report;
    }


    public List<SystemReportItem> alarmsPerSystem(LocalDateTime start_time, LocalDateTime end_time) {
        List<SystemReportItem> report = new LinkedList<>();


        Map<Software, Integer> counter = new HashMap<>();
        for (Alarm alarm: this.alarmService.findAll()) {
            for (Log log: alarm.getAssociatedLogs()) {
                if (log.getTimestamp().getValue().isAfter(start_time) && log.getTimestamp().getValue().isBefore(end_time)) {
                    counter.put(log.getSoftware(), counter.getOrDefault(log.getSoftware(), 0) + 1);
                }
            }
        }

        for (Map.Entry<Software, Integer> entry: counter.entrySet()) {
            report.add(new SystemReportItem(entry.getKey().getValue(), entry.getValue()));
        }

        return report;
    }
}
