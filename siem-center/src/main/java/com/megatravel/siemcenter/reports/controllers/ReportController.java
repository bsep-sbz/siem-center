package com.megatravel.siemcenter.reports.controllers;

import com.megatravel.siemcenter.common.util.Timestamp;
import com.megatravel.siemcenter.reports.models.MachineReportItem;
import com.megatravel.siemcenter.reports.models.SystemReportItem;
import com.megatravel.siemcenter.reports.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.time.LocalDateTime;
import java.util.List;


@RestController
@RequestMapping("/reports")
public class ReportController {

    private final ReportService reportService;

    @Autowired
    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("/logs-per-machine")
    public List<MachineReportItem> getLogsPerMachine(@PathParam("start_time") String start_time, @PathParam("end_time") String end_time) {
        return this.reportService.logsPerMachine(
            Timestamp.parse(start_time).getValue(),
            Timestamp.parse(end_time).getValue()
        );
    }

    @GetMapping("/logs-per-system")
    public List<SystemReportItem> getLogsPerSystem(@PathParam("start_time") String start_time, @PathParam("end_time") String end_time) {
        return this.reportService.logsPerSystem(
            Timestamp.parse(start_time).getValue(),
            Timestamp.parse(end_time).getValue()
        );
    }

    @GetMapping("/alarms-per-machine")
    public List<MachineReportItem> getAlarmsPerMachine(@PathParam("start_time") String start_time, @PathParam("end_time") String end_time) {
        return this.reportService.alarmsPerMachine(
            Timestamp.parse(start_time).getValue(),
            Timestamp.parse(end_time).getValue()
        );
    }

    @GetMapping("/alarms-per-system")
    public List<SystemReportItem> getAlarmsPerSystem(@PathParam("start_time") String start_time, @PathParam("end_time") String end_time) {
        return this.reportService.alarmsPerSystem(
            Timestamp.parse(start_time).getValue(),
            Timestamp.parse(end_time).getValue()
        );
    }
}
