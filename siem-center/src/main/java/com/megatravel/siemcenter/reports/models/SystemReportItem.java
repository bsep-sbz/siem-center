package com.megatravel.siemcenter.reports.models;

import com.megatravel.siemcenter.common.models.Software;

public class SystemReportItem {
    private String software;
    private int count;

    public SystemReportItem(String software, int count) {
        this.software = software;
        this.count = count;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
